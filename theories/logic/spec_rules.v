(* ReLoC -- Relational logic for fine-grained concurrency *)
(** Rules for updating the specification program. *)
From iris.algebra Require Import auth excl frac agree gmap list.
From iris.proofmode Require Import proofmode.
From iris.heap_lang Require Export lang notation tactics.
From reloc.logic Require Export spec_ra.
Import uPred.

Section rules.
  Context `{relocG Σ}.
  Implicit Types P Q : iProp Σ.
  Implicit Types Φ : val → iProp Σ.
  Implicit Types σ : state.
  Implicit Types e : expr.
  Implicit Types v : val.
  Implicit Types l : loc.

  Local Hint Resolve tpool_lookup : core.
  Local Hint Resolve tpool_lookup_Some : core.
  Local Hint Resolve to_tpool_insert : core.
  Local Hint Resolve to_tpool_insert' : core.
  Local Hint Resolve tpool_singleton_included : core.

  (** * Aux. lemmas *)
  Lemma step_insert K tp j e σ κ e' σ' efs :
    tp !! j = Some (fill K e) → head_step e σ κ e' σ' efs →
    erased_step (tp, σ) (<[j:=fill K e']> tp ++ efs, σ').
  Proof.
    intros. rewrite -(take_drop_middle tp j (fill K e)) //.
    rewrite insert_app_r_alt take_length_le ?Nat.sub_diag /=;
      eauto using lookup_lt_Some, Nat.lt_le_incl.
    rewrite -(assoc_L (++)) /=. eexists.
    eapply step_atomic; eauto. by apply: Ectx_step'.
  Qed.

  Lemma step_insert_no_fork K tp j e σ κ e' σ' :
    tp !! j = Some (fill K e) → head_step e σ κ e' σ' [] →
    erased_step (tp, σ) (<[j:=fill K e']> tp, σ').
  Proof. rewrite -(right_id_L [] (++) (<[_:=_]>_)). by apply step_insert. Qed.

  (** * Main rules *)
  (** Pure reductions *)
  Lemma step_pure E j K e e' (P : Prop) n :
    P →
    PureExec P n e e' →
    nclose specN ⊆ E →
    spec_ctx ∗ j ⤇ fill K e ={E}=∗ spec_ctx ∗ j ⤇ fill K e'.
  Proof.
    iIntros (HP Hex ?) "[#Hspec Hj]". iFrame "Hspec".
    rewrite /spec_ctx tpool_mapsto_eq /tpool_mapsto_def /=.
    iDestruct "Hspec" as (ρ) "Hspec".
    iInv specN as (tp σ) ">[Hown Hrtc]" "Hclose".
    iDestruct "Hrtc" as %Hrtc.
    iDestruct (own_valid_2 with "Hown Hj")
      as %[[Htpj%tpool_singleton_included' _]%prod_included ?]%auth_both_valid_discrete.
    iMod (own_update_2 with "Hown Hj") as "[Hown Hj]".
    { eapply auth_update, prod_local_update_1.
      by eapply (singleton_local_update _ j (Excl (fill K e))),
        (exclusive_local_update _ (Excl (fill K e'))). }
    iFrame "Hj". iApply "Hclose". iNext. iExists (<[j:=fill K e']> tp), σ.
    rewrite to_tpool_insert'; last eauto.
    iFrame. iPureIntro.
    apply rtc_nsteps_1 in Hrtc; destruct Hrtc as [m Hrtc].
    specialize (Hex HP). apply (rtc_nsteps_2 (m + n)).
    eapply nsteps_trans; eauto.
    revert e e' Htpj Hex.
    induction n => e e' Htpj Hex.
    - inversion Hex; subst.
      rewrite list_insert_id; eauto. econstructor.
    - apply nsteps_inv_r in Hex.
      destruct Hex as [z [Hex1 Hex2]].
      specialize (IHn _ _ Htpj Hex1).
      eapply nsteps_r; eauto.
      replace (<[j:=fill K e']> tp) with
          (<[j:=fill K e']> (<[j:=fill K z]> tp)); last first.
      { clear. revert tp; induction j; intros tp.
        - destruct tp; trivial.
        - destruct tp; simpl; auto. by rewrite IHj. }
      destruct Hex2 as [Hexs Hexd].
      specialize (Hexs σ). destruct Hexs as [e'' [σ' [efs Hexs]]].
      specialize (Hexd σ [] e'' σ' efs Hexs); destruct Hexd as [? [? [? ?]]];
        subst.
      inversion Hexs; simpl in *; subst.
      rewrite -!fill_app.
      eapply step_insert_no_fork; eauto.
      { apply list_lookup_insert. apply lookup_lt_is_Some; eauto. }
  Qed.

  (** Prophecy variables (at this point those are just noops) *)
  Lemma step_newproph E j K :
    nclose specN ⊆ E →
    spec_ctx ∗ j ⤇ fill K NewProph ={E}=∗
    ∃ (p : proph_id), spec_ctx ∗ j ⤇ fill K #p.
  Proof.
    iIntros (?) "[#Hinv Hj]". iFrame "Hinv".
    rewrite /spec_ctx tpool_mapsto_eq /tpool_mapsto_def /=.
    iDestruct "Hinv" as (ρ) "Hinv".
    iInv specN as (tp σ) ">[Hown %]" "Hclose".
    iDestruct (own_valid_2 with "Hown Hj")
      as %[[?%tpool_singleton_included' _]%prod_included _]%auth_both_valid_discrete.
    destruct (exist_fresh (used_proph_id σ)) as [p Hp].
    iMod (own_update_2 with "Hown Hj") as "[Hown Hj]".
    { by eapply auth_update, prod_local_update_1,
         singleton_local_update, (exclusive_local_update _ (Excl (fill K #p))). }
    iExists p. iFrame. iApply "Hclose". iNext.
    iExists (<[j:=fill K #p]> tp), (state_upd_used_proph_id ({[ p ]} ∪.) σ).
    rewrite to_tpool_insert'; last eauto. iFrame. iPureIntro.
    eapply rtc_r, step_insert_no_fork; eauto. econstructor; eauto.
  Qed.

  Lemma step_resolveproph E j K (p : proph_id) w :
    nclose specN ⊆ E →
    spec_ctx ∗ j ⤇ fill K (ResolveProph #p (of_val w)) ={E}=∗
    spec_ctx ∗ j ⤇ fill K #().
  Proof.
    iIntros (?) "[#Hinv Hj]". iFrame "Hinv".
    rewrite /spec_ctx tpool_mapsto_eq /tpool_mapsto_def /=.
    iDestruct "Hinv" as (ρ) "Hinv".
    iInv specN as (tp σ) ">[Hown %]" "Hclose".
    iDestruct (own_valid_2 with "Hown Hj")
      as %[[?%tpool_singleton_included' _]%prod_included _]%auth_both_valid_discrete.
    iMod (own_update_2 with "Hown Hj") as "[Hown Hj]".
    { by eapply auth_update, prod_local_update_1,
         singleton_local_update, (exclusive_local_update _ (Excl (fill K #()))). }
    iFrame. iApply "Hclose". iNext.
    iExists (<[j:=fill K #()]> tp), σ.
    rewrite to_tpool_insert'; last eauto. iFrame. iPureIntro.
    eapply rtc_r, step_insert_no_fork; eauto. do 2 econstructor; eauto.
  Qed.

  Lemma step_resolveproph_atomic E j K (p : proph_id) w e v :
    nclose specN ⊆ E →
    (forall σ, exists κs σ', (to_heap (heap σ), ∅) ~l~> (to_heap (heap σ'), ∅) ∧ head_step e σ κs v σ' []) →
    spec_ctx ∗ j ⤇ fill K (Resolve e #p (of_val w)) ={E}=∗
    spec_ctx ∗ j ⤇ fill K v.
  Proof.
    iIntros (? step) "[#Hinv Hj]". iFrame "Hinv".
    rewrite /spec_ctx tpool_mapsto_eq /tpool_mapsto_def /=.
    iDestruct "Hinv" as (ρ) "Hinv".
    iInv specN as (tp σ) ">[Hown %]" "Hclose".
    iDestruct (own_valid_2 with "Hown Hj")
      as %[[?%tpool_singleton_included' _]%prod_included _]%auth_both_valid_discrete.
    destruct (step σ) as [κs [σ' step']].
    iMod (own_update_2 with "Hown Hj") as "[Hown Hj]".
    { by eapply auth_update, prod_local_update_1,
         singleton_local_update, (exclusive_local_update _ (Excl (fill K v))). }
    iMod (own_update_2 with "Hown Hj") as "[Hown Hj]".
    { eapply auth_update, prod_local_update_2. apply step'. }
    iFrame. iApply "Hclose". iNext.
    iExists (<[j:=fill K v]> tp), σ'.
    rewrite to_tpool_insert'; last eauto. iFrame. iPureIntro.
    eapply rtc_r, step_insert_no_fork; eauto. specialize ResolveS as h. econstructor. apply step'.
  Qed.

  (** Alloc, load, and store *)
  Lemma step_alloc E j K e v :
    IntoVal e v →
    nclose specN ⊆ E →
    spec_ctx ∗ j ⤇ fill K (ref e) ={E}=∗ ∃ l, spec_ctx ∗ j ⤇ fill K (#l) ∗ l ↦ₛ v.
  Proof.
    iIntros (<-?) "[#Hinv Hj]". iFrame "Hinv".
    rewrite /spec_ctx tpool_mapsto_eq /tpool_mapsto_def /=.
    iDestruct "Hinv" as (ρ) "Hinv".
    iInv specN as (tp σ) ">[Hown %]" "Hclose".
    destruct (exist_fresh (dom (heap σ))) as [l Hl%not_elem_of_dom].
    iDestruct (own_valid_2 with "Hown Hj")
      as %[[?%tpool_singleton_included' _]%prod_included ?]%auth_both_valid_discrete.
    iMod (own_update_2 with "Hown Hj") as "[Hown Hj]".
    { by eapply auth_update, prod_local_update_1,
        singleton_local_update, (exclusive_local_update _ (Excl (fill K (#l)%E))). }
    iMod (own_update with "Hown") as "[Hown Hl]".
    { eapply auth_update_alloc, prod_local_update_2,
        (alloc_singleton_local_update _ l (1%Qp,to_agree (Some v : leibnizO _))); last done.
      by apply lookup_to_heap_None. }
    rewrite heapS_mapsto_eq /heapS_mapsto_def /=.
    iExists l. iFrame "Hj Hl". iApply "Hclose". iNext.
    iExists (<[j:=fill K (# l)]> tp), (state_upd_heap <[l:=Some v]> σ).
    rewrite to_heap_insert to_tpool_insert'; last eauto. iFrame. iPureIntro.
    eapply rtc_r, step_insert_no_fork; eauto.
    rewrite -state_init_heap_singleton. eapply AllocNS; first by lia.
    intros. assert (i = 0) as -> by lia. by rewrite loc_add_0.
  Qed.

  Lemma step_load E j K l q v:
    nclose specN ⊆ E →
    spec_ctx ∗ j ⤇ fill K (!#l) ∗ l ↦ₛ{q} v
    ={E}=∗ spec_ctx ∗ j ⤇ fill K (of_val v) ∗ l ↦ₛ{q} v.
  Proof.
    iIntros (?) "(#Hinv & Hj & Hl)". iFrame "Hinv".
    rewrite /spec_ctx tpool_mapsto_eq /tpool_mapsto_def.
    iDestruct "Hinv" as (ρ) "Hinv".
    rewrite heapS_mapsto_eq /heapS_mapsto_def /=.
    iInv specN as (tp σ) ">[Hown %]" "Hclose".
    iDestruct (own_valid_2 with "Hown Hj")
      as %[[?%tpool_singleton_included' _]%prod_included ?]%auth_both_valid_discrete.
    iDestruct (own_valid_2 with "Hown Hl")
      as %[[? ?%heap_singleton_included]%prod_included ?]%auth_both_valid_discrete.
    iMod (own_update_2 with "Hown Hj") as "[Hown Hj]".
    { by eapply auth_update, prod_local_update_1, singleton_local_update,
        (exclusive_local_update _ (Excl (fill K (of_val v)))). }
    iFrame "Hj Hl". iApply "Hclose". iNext.
    iExists (<[j:=fill K (of_val v)]> tp), σ.
    rewrite to_tpool_insert'; last eauto. iFrame. iPureIntro.
    eapply rtc_r, step_insert_no_fork; eauto. econstructor; eauto.
  Qed.

  Lemma step_store E j K l v' e v:
    IntoVal e v →
    nclose specN ⊆ E →
    spec_ctx ∗ j ⤇ fill K (#l <- e) ∗ l ↦ₛ v'
    ={E}=∗ spec_ctx ∗ j ⤇ fill K #() ∗ l ↦ₛ v.
  Proof.
    iIntros (<-?) "(#Hinv & Hj & Hl)". iFrame "Hinv".
    rewrite /spec_ctx tpool_mapsto_eq /tpool_mapsto_def.
    iDestruct "Hinv" as (ρ) "Hinv".
    rewrite heapS_mapsto_eq /heapS_mapsto_def /=.
    iInv specN as (tp σ) ">[Hown %]" "Hclose".
    iDestruct (own_valid_2 with "Hown Hj")
      as %[[?%tpool_singleton_included' _]%prod_included _]%auth_both_valid_discrete.
    iDestruct (own_valid_2 with "Hown Hl")
      as %[[_ Hl%heap_singleton_included]%prod_included _]%auth_both_valid_discrete.
    iMod (own_update_2 with "Hown Hj") as "[Hown Hj]".
    { by eapply auth_update, prod_local_update_1, singleton_local_update,
        (exclusive_local_update _ (Excl (fill K #()))). }
    iMod (own_update_2 with "Hown Hl") as "[Hown Hl]".
    { eapply auth_update, prod_local_update_2.
      apply: singleton_local_update.
      { by rewrite /to_heap lookup_fmap Hl. }
      apply: (exclusive_local_update _
        (1%Qp, to_agree (Some v : leibnizO (option val)))).
      apply: pair_exclusive_l. done. }
    iFrame "Hj Hl". iApply "Hclose". iNext.
    iExists (<[j:=fill K #()]> tp), (state_upd_heap <[l:=Some v]> σ).
    rewrite to_heap_insert to_tpool_insert'; last eauto. iFrame. iPureIntro.
    eapply rtc_r, step_insert_no_fork; eauto. econstructor; eauto.
  Qed.

  Lemma step_xchg E j K l e (v v' : val) :
    IntoVal e v →
    nclose specN ⊆ E →
    spec_ctx ∗ j ⤇ fill K (Xchg #l e) ∗ l ↦ₛ v'
    ={E}=∗ spec_ctx ∗ j ⤇ fill K (of_val v') ∗ l ↦ₛ v.
  Proof.
    iIntros (<-?) "(#Hinv & Hj & Hl)". iFrame "Hinv".
    rewrite /spec_ctx tpool_mapsto_eq /tpool_mapsto_def.
    iDestruct "Hinv" as (ρ) "Hinv".
    rewrite heapS_mapsto_eq /heapS_mapsto_def /=.
    iInv specN as (tp σ) ">[Hown %]" "Hclose".
    iDestruct (own_valid_2 with "Hown Hj")
      as %[[?%tpool_singleton_included' _]%prod_included _]%auth_both_valid_discrete.
    iDestruct (own_valid_2 with "Hown Hl")
      as %[[_ Hl%heap_singleton_included]%prod_included _]%auth_both_valid_discrete.
    iMod (own_update_2 with "Hown Hj") as "[Hown Hj]".
    { by eapply auth_update, prod_local_update_1, singleton_local_update,
        (exclusive_local_update _ (Excl (fill K (of_val v')))). }
    iMod (own_update_2 with "Hown Hl") as "[Hown Hl]".
    { eapply auth_update, prod_local_update_2.
      apply: singleton_local_update.
      { by rewrite /to_heap lookup_fmap Hl. }
      apply: (exclusive_local_update _
        (1%Qp, to_agree (Some v : leibnizO (option val)))).
      apply: pair_exclusive_l. done. }
    iFrame "Hj Hl". iApply "Hclose". iNext.
    iExists (<[j:=fill K (of_val v')]> tp), (state_upd_heap <[l:=Some v]> σ).
    rewrite to_heap_insert to_tpool_insert'; last eauto. iFrame. iPureIntro.
    eapply rtc_r, step_insert_no_fork; eauto. econstructor; eauto.
  Qed.

  (** CmpXchg & FAA *)
  Lemma step_cmpxchg_fail E j K l q v' e1 v1 e2 v2 :
    IntoVal e1 v1 →
    IntoVal e2 v2 →
    nclose specN ⊆ E →
    vals_compare_safe v' v1 →
    v' ≠ v1 →
    spec_ctx ∗ j ⤇ fill K (CmpXchg #l e1 e2) ∗ l ↦ₛ{q} v'
    ={E}=∗ spec_ctx ∗ j ⤇ fill K (v', #false)%V ∗ l ↦ₛ{q} v'.
  Proof.
    iIntros (<-<-???) "(#Hinv & Hj & Hl)". iFrame "Hinv".
    rewrite /spec_ctx tpool_mapsto_eq /tpool_mapsto_def heapS_mapsto_eq /heapS_mapsto_def.
    iDestruct "Hinv" as (ρ) "Hinv".
    iInv specN as (tp σ) ">[Hown %]" "Hclose".
    iDestruct (own_valid_2 with "Hown Hj")
      as %[[?%tpool_singleton_included' _]%prod_included ?]%auth_both_valid_discrete.
    iDestruct (own_valid_2 with "Hown Hl")
      as %[[_ ?%heap_singleton_included]%prod_included _]%auth_both_valid_discrete.
    iMod (own_update_2 with "Hown Hj") as "[Hown Hj]".
    { by eapply auth_update, prod_local_update_1, singleton_local_update,
        (exclusive_local_update _ (Excl (fill K (_, #false)%V))). }
    iFrame "Hj Hl". iApply "Hclose". iNext.
    iExists (<[j:=fill K (_, #false)%V]> tp), σ.
    rewrite to_tpool_insert'; last eauto. iFrame. iPureIntro.
    eapply rtc_r, step_insert_no_fork; eauto. econstructor; eauto.
    rewrite bool_decide_false //.
  Qed.

  Lemma step_cmpxchg_suc E j K l e1 v1 v1' e2 v2:
    IntoVal e1 v1 →
    IntoVal e2 v2 →
    nclose specN ⊆ E →
    vals_compare_safe v1' v1 →
    v1' = v1 →
    spec_ctx ∗ j ⤇ fill K (CmpXchg #l e1 e2) ∗ l ↦ₛ v1'
    ={E}=∗ spec_ctx ∗ j ⤇ fill K (v1', #true)%V ∗ l ↦ₛ v2.
  Proof.
    iIntros (<-<-???) "(#Hinv & Hj & Hl)"; subst. iFrame "Hinv".
    rewrite /spec_ctx tpool_mapsto_eq /tpool_mapsto_def heapS_mapsto_eq /heapS_mapsto_def.
    iDestruct "Hinv" as (ρ) "Hinv".
    iInv specN as (tp σ) ">[Hown %]" "Hclose".
    iDestruct (own_valid_2 with "Hown Hj")
      as %[[?%tpool_singleton_included' _]%prod_included _]%auth_both_valid_discrete.
    iDestruct (own_valid_2 with "Hown Hl")
      as %[[_ Hl%heap_singleton_included]%prod_included _]%auth_both_valid_discrete.
    iMod (own_update_2 with "Hown Hj") as "[Hown Hj]".
    { by eapply auth_update, prod_local_update_1, singleton_local_update,
        (exclusive_local_update _ (Excl (fill K (_, #true)%V))). }
    iMod (own_update_2 with "Hown Hl") as "[Hown Hl]".
    { eapply auth_update, prod_local_update_2.
      apply: singleton_local_update.
      { by rewrite /to_heap lookup_fmap Hl. }
      apply: (exclusive_local_update _
        (1%Qp, to_agree (Some v2 : leibnizO (option val)))).
      apply: pair_exclusive_l. done. }
    iFrame "Hj Hl". iApply "Hclose". iNext.
    iExists (<[j:=fill K (_, #true)%V]> tp), (state_upd_heap <[l:=Some v2]> σ).
    rewrite to_heap_insert to_tpool_insert'; last eauto. iFrame. iPureIntro.
    eapply rtc_r, step_insert_no_fork; eauto. econstructor; eauto.
    rewrite bool_decide_true //.
  Qed.

  Lemma step_resolve_cmpxchg_fail E j K l q v' e1 v1 e2 v2 (p : proph_id) (w : val):
    IntoVal e1 v1 →
    IntoVal e2 v2 →
    nclose specN ⊆ E →
    vals_compare_safe v' v1 →
    v' ≠ v1 →
    spec_ctx ∗ j ⤇ fill K (Resolve (CmpXchg #l e1 e2) #p w) ∗ l ↦ₛ{q} v'
    ={E}=∗ spec_ctx ∗ j ⤇ fill K (v', #false)%V ∗ l ↦ₛ{q} v'.
  Proof.
    iIntros (<-<-???) "(#Hinv & Hj & Hl)". iFrame "Hinv".
    rewrite /spec_ctx tpool_mapsto_eq /tpool_mapsto_def heapS_mapsto_eq /heapS_mapsto_def.
    iDestruct "Hinv" as (ρ) "Hinv".
    iInv specN as (tp σ) ">[Hown %]" "Hclose".
    iDestruct (own_valid_2 with "Hown Hj")
      as %[[?%tpool_singleton_included' _]%prod_included ?]%auth_both_valid_discrete.
    iDestruct (own_valid_2 with "Hown Hl")
      as %[[_ ?%heap_singleton_included]%prod_included _]%auth_both_valid_discrete.
    iMod (own_update_2 with "Hown Hj") as "[Hown Hj]".
    { by eapply auth_update, prod_local_update_1, singleton_local_update,
        (exclusive_local_update _ (Excl (fill K (_, #false)%V))). }
    iFrame "Hj Hl". iApply "Hclose". iNext.
    iExists (<[j:=fill K (_, #false)%V]> tp), σ.
    rewrite to_tpool_insert'; last eauto. iFrame. iPureIntro.
    eapply rtc_r, step_insert_no_fork; eauto. do 2 econstructor; eauto.
    rewrite bool_decide_false //.
  Qed.

  Lemma step_resolve_cmpxchg_suc E j K l e1 v1 v1' e2 v2 (p : proph_id) (w : val):
    IntoVal e1 v1 →
    IntoVal e2 v2 →
    nclose specN ⊆ E →
    vals_compare_safe v1' v1 →
    v1' = v1 →
    spec_ctx ∗ j ⤇ fill K (Resolve (CmpXchg #l e1 e2) #p w) ∗ l ↦ₛ v1'
    ={E}=∗ spec_ctx ∗ j ⤇ fill K (v1', #true)%V ∗ l ↦ₛ v2.
  Proof.
    iIntros (<-<-???) "(#Hinv & Hj & Hl)"; subst. iFrame "Hinv".
    rewrite /spec_ctx tpool_mapsto_eq /tpool_mapsto_def heapS_mapsto_eq /heapS_mapsto_def.
    iDestruct "Hinv" as (ρ) "Hinv".
    iInv specN as (tp σ) ">[Hown %]" "Hclose".
    iDestruct (own_valid_2 with "Hown Hj")
      as %[[?%tpool_singleton_included' _]%prod_included _]%auth_both_valid_discrete.
    iDestruct (own_valid_2 with "Hown Hl")
      as %[[_ Hl%heap_singleton_included]%prod_included _]%auth_both_valid_discrete.
    iMod (own_update_2 with "Hown Hj") as "[Hown Hj]".
    { by eapply auth_update, prod_local_update_1, singleton_local_update,
        (exclusive_local_update _ (Excl (fill K (_, #true)%V))). }
    iMod (own_update_2 with "Hown Hl") as "[Hown Hl]".
    { eapply auth_update, prod_local_update_2.
      apply: singleton_local_update.
      { by rewrite /to_heap lookup_fmap Hl. }
      apply: (exclusive_local_update _
        (1%Qp, to_agree (Some v2 : leibnizO (option val)))).
      apply: pair_exclusive_l. done. }
    iFrame "Hj Hl". iApply "Hclose". iNext.
    iExists (<[j:=fill K (_, #true)%V]> tp), (state_upd_heap <[l:=Some v2]> σ).
    rewrite to_heap_insert to_tpool_insert'; last eauto. iFrame. iPureIntro.
    eapply rtc_r, step_insert_no_fork; eauto. econstructor. econstructor; eauto.
    rewrite bool_decide_true //.
  Qed.

  Lemma step_faa E j K l e1 e2 (i1 i2 : Z) :
    IntoVal e1 #i2 →
    nclose specN ⊆ E →
    spec_ctx ∗ j ⤇ fill K (FAA #l e1) ∗ l ↦ₛ #i1
    ={E}=∗ spec_ctx ∗ j ⤇ fill K #i1 ∗ l ↦ₛ #(i1+i2).
  Proof.
    iIntros (<-?) "(#Hinv & Hj & Hl)"; subst. iFrame "Hinv".
    rewrite /spec_ctx tpool_mapsto_eq /tpool_mapsto_def heapS_mapsto_eq /heapS_mapsto_def.
    iDestruct "Hinv" as (ρ) "Hinv".
    iInv specN as (tp σ) ">[Hown %]" "Hclose".
    iDestruct (own_valid_2 with "Hown Hj")
      as %[[?%tpool_singleton_included' _]%prod_included _]%auth_both_valid_discrete.
    iDestruct (own_valid_2 with "Hown Hl")
      as %[[_ Hl%heap_singleton_included]%prod_included _]%auth_both_valid_discrete.
    iMod (own_update_2 with "Hown Hj") as "[Hown Hj]".
    { by eapply auth_update, prod_local_update_1, singleton_local_update,
        (exclusive_local_update _ (Excl (fill K (# i1)))). }
    iMod (own_update_2 with "Hown Hl") as "[Hown Hl]".
    { eapply auth_update, prod_local_update_2.
      apply: singleton_local_update.
      { by rewrite /to_heap lookup_fmap Hl. }
      apply: (exclusive_local_update _
        (1%Qp, to_agree (Some #(i1+i2) : leibnizO (option val)))).
      apply: pair_exclusive_l. done. }
    iFrame "Hj Hl". iApply "Hclose". iNext.
    iExists (<[j:=fill K (# i1)]> tp), (state_upd_heap <[l:=Some #(i1+i2)]> σ).
    rewrite to_heap_insert to_tpool_insert'; last eauto. iFrame. iPureIntro.
    eapply rtc_r, step_insert_no_fork; eauto. simpl. econstructor; eauto.
  Qed.

  (** Fork *)
  Lemma step_fork E j K e :
    nclose specN ⊆ E →
    spec_ctx ∗ j ⤇ fill K (Fork e) ={E}=∗
    ∃ j', (spec_ctx ∗ j ⤇ fill K #()) ∗ (spec_ctx ∗ j' ⤇ e).
  Proof.
    iIntros (?) "[#Hspec Hj]". iFrame "Hspec".
    rewrite /spec_ctx tpool_mapsto_eq /tpool_mapsto_def.
    iDestruct "Hspec" as (ρ) "Hspec".
    iInv specN as (tp σ) ">[Hown %]" "Hclose".
    iDestruct (own_valid_2 with "Hown Hj")
      as %[[?%tpool_singleton_included' _]%prod_included ?]%auth_both_valid_discrete.
    assert (j < length tp) by eauto using lookup_lt_Some.
    iMod (own_update_2 with "Hown Hj") as "[Hown Hj]".
    { by eapply auth_update, prod_local_update_1,
        singleton_local_update, (exclusive_local_update _ (Excl (fill K #()))). }
    iMod (own_update with "Hown") as "[Hown Hfork]".
    { eapply auth_update_alloc, prod_local_update_1,
        (alloc_singleton_local_update _ (length tp) (Excl e)); last done.
      rewrite lookup_insert_ne ?tpool_lookup; last lia.
      by rewrite lookup_ge_None_2. }
    iExists (length tp). iFrame "Hj Hfork". iApply "Hclose". iNext.
    iExists (<[j:=fill K #()]> tp ++ [e]), σ.
    rewrite to_tpool_snoc insert_length to_tpool_insert //. iFrame. iPureIntro.
    eapply rtc_r, step_insert; eauto. econstructor; eauto.
  Qed.

  Fixpoint is_list_s (hd: loc) (xs: list val) : iProp Σ :=
    match xs with
    | [] => hd ↦ₛ NONEV
    | x :: xs => ∃ hd': loc, hd ↦ₛ SOMEV (x, #hd') ∗ is_list_s hd' xs
    end.

  Definition is_stack_s (s: loc) xs: iProp Σ := (∃ hd: loc, s ↦ₛ #hd ∗ is_list_s hd xs)%I.

  Global Instance is_list_s_timeless xs hd: Timeless (is_list_s hd xs).
  Proof. generalize hd. induction xs; apply _. Qed.

  Global Instance is_stack_s_timeless xs hd: Timeless (is_stack_s hd xs).
  Proof. generalize hd. induction xs; apply _. Qed.

  Lemma step_push_prim E j K l e v xs :
    IntoVal e v →
    nclose specN ⊆ E →
    spec_ctx ∗ j ⤇ fill K (Push_prim #l e) ∗ is_stack_s l xs
    ={E}=∗ spec_ctx ∗ j ⤇ fill K #() ∗ is_stack_s l (v::xs).
  Proof.
    iIntros (<-?) "(#Hinv & Hj & %hd & Hl & Hhd)". iFrame "Hinv".
    rewrite /is_stack_s /spec_ctx tpool_mapsto_eq /tpool_mapsto_def /=.
    iDestruct "Hinv" as (ρ) "Hinv".
    rewrite heapS_mapsto_eq /heapS_mapsto_def /=.
    iInv specN as (tp σ) ">[Hown %]" "Hclose".
    iDestruct (own_valid_2 with "Hown Hj")
      as %[[?%tpool_singleton_included' _]%prod_included _]%auth_both_valid_discrete.
    iDestruct (own_valid_2 with "Hown Hl")
      as %[[_ Hl%heap_singleton_included]%prod_included _]%auth_both_valid_discrete.
    iMod (own_update_2 with "Hown Hj") as "[Hown Hj]".
    { by eapply auth_update, prod_local_update_1, singleton_local_update,
        (exclusive_local_update _ (Excl (fill K #()))). }
    destruct (exist_fresh (dom (heap σ))) as [l' Hl'%not_elem_of_dom].
    iMod (own_update with "Hown") as "[Hown Hl']".
    { eapply auth_update_alloc, prod_local_update_2,
        (alloc_singleton_local_update _ l' (1%Qp,to_agree (Some (InjRV(v, #hd)) : leibnizO _))); last done.
      by apply lookup_to_heap_None. }
    iAssert (is_list_s l' (v::xs)) with "[Hhd Hl']" as "Hl'"; first by (iExists _; iFrame; rewrite heapS_mapsto_eq).
    iMod (own_update_2 with "Hown Hl") as "[Hown Hl]".
    { eapply auth_update, prod_local_update_2.
      apply: singleton_local_update.
      { rewrite /to_heap lookup_insert_ne; first by rewrite lookup_fmap Hl. intros ->; rewrite Hl in Hl'; discriminate. }
      apply: (exclusive_local_update _
        (1%Qp, to_agree (Some #l' : leibnizO (option val)))).
      apply: pair_exclusive_l. done. }
    iFrame "Hj". iExists l'. iFrame.
    unfold is_list_s. rewrite heapS_mapsto_eq /heapS_mapsto_def /=. iFrame.
    iApply "Hclose". iNext.
    iExists (<[j:=fill K #()]> tp), (state_upd_heap <[l:=Some #l']> (state_upd_heap <[l':=Some (InjRV (v, #hd))]> σ)).
    rewrite to_heap_insert to_tpool_insert'; last eauto.
    rewrite to_heap_insert. iFrame. iPureIntro.
    eapply rtc_r, step_insert_no_fork; eauto. econstructor; eauto.
  Qed.

  Lemma step_push_prim' E j K l e v (hd : loc) :
    IntoVal e v →
    nclose specN ⊆ E →
    spec_ctx ∗ j ⤇ fill K (Push_prim #l e) ∗ l ↦ₛ #hd
    ={E}=∗ spec_ctx ∗ j ⤇ fill K #() ∗ (∃ (l' : loc), l ↦ₛ #l' ∗ l' ↦ₛ InjRV(v, #hd)%V).
  Proof.
    iIntros (<-?) "(#Hinv & Hj & Hl)". iFrame "Hinv".
    rewrite /is_stack_s /spec_ctx tpool_mapsto_eq /tpool_mapsto_def /=.
    iDestruct "Hinv" as (ρ) "Hinv".
    rewrite heapS_mapsto_eq /heapS_mapsto_def /=.
    iInv specN as (tp σ) ">[Hown %]" "Hclose".
    iDestruct (own_valid_2 with "Hown Hj")
      as %[[?%tpool_singleton_included' _]%prod_included _]%auth_both_valid_discrete.
    iDestruct (own_valid_2 with "Hown Hl")
      as %[[_ Hl%heap_singleton_included]%prod_included _]%auth_both_valid_discrete.
    iMod (own_update_2 with "Hown Hj") as "[Hown Hj]".
    { by eapply auth_update, prod_local_update_1, singleton_local_update,
        (exclusive_local_update _ (Excl (fill K #()))). }
    destruct (exist_fresh (dom (heap σ))) as [l' Hl'%not_elem_of_dom].
    iMod (own_update with "Hown") as "[Hown Hl']".
    { eapply auth_update_alloc, prod_local_update_2,
        (alloc_singleton_local_update _ l' (1%Qp,to_agree (Some (InjRV(v, #hd)) : leibnizO _))); last done.
      by apply lookup_to_heap_None. }
    iMod (own_update_2 with "Hown Hl") as "[Hown Hl]".
    { eapply auth_update, prod_local_update_2.
      apply: singleton_local_update.
      { rewrite /to_heap lookup_insert_ne; first by rewrite lookup_fmap Hl. intros ->; rewrite Hl in Hl'; discriminate. }
      apply: (exclusive_local_update _
        (1%Qp, to_agree (Some #l' : leibnizO (option val)))).
      apply: pair_exclusive_l. done. }
    iFrame "Hj". iExists l'. iFrame.
    iApply "Hclose". iNext.
    iExists (<[j:=fill K #()]> tp), (state_upd_heap <[l:=Some #l']> (state_upd_heap <[l':=Some (InjRV (v, #hd))]> σ)).
    rewrite to_heap_insert to_tpool_insert'; last eauto.
    rewrite to_heap_insert. iFrame. iPureIntro.
    eapply rtc_r, step_insert_no_fork; eauto. econstructor; eauto.
  Qed.

  Lemma step_pop_prim_empty E j K l :
    nclose specN ⊆ E →
    spec_ctx ∗ j ⤇ fill K (Pop_prim #l) ∗ is_stack_s l []
    ={E}=∗ spec_ctx ∗ j ⤇ fill K (InjLV #()) ∗ is_stack_s l [].
  Proof.
    iIntros (?) "(#Hinv & Hj & %hd & Hl & Hhd)". cbn. iFrame "Hinv".
    rewrite /is_stack_s /spec_ctx tpool_mapsto_eq /tpool_mapsto_def /=.
    iDestruct "Hinv" as (ρ) "Hinv".
    rewrite heapS_mapsto_eq /heapS_mapsto_def /=.
    iInv specN as (tp σ) ">[Hown %]" "Hclose".
    iDestruct (own_valid_2 with "Hown Hj")
      as %[[?%tpool_singleton_included' _]%prod_included _]%auth_both_valid_discrete.
    iDestruct (own_valid_2 with "Hown Hl")
      as %[[_ Hl%heap_singleton_included]%prod_included _]%auth_both_valid_discrete.
    iDestruct (own_valid_2 with "Hown Hhd")
      as %[[_ Hhd%heap_singleton_included]%prod_included _]%auth_both_valid_discrete.
    iMod (own_update_2 with "Hown Hj") as "[Hown Hj]".
    { by eapply auth_update, prod_local_update_1, singleton_local_update,
        (exclusive_local_update _ (Excl (fill K (InjLV #())))). }
    iFrame "Hj". iExists hd. iFrame.
    iApply "Hclose". iNext.
    iExists (<[j:=fill K (InjLV #())]> tp), σ.
    rewrite to_tpool_insert'; last eauto. iFrame. iPureIntro.
    eapply rtc_r, step_insert_no_fork; eauto. econstructor; eauto.
  Qed.

  Lemma step_pop_prim_succ E j K l x xs :
    nclose specN ⊆ E →
    spec_ctx ∗ j ⤇ fill K (Pop_prim #l) ∗ is_stack_s l (x::xs)
    ={E}=∗ spec_ctx ∗ j ⤇ fill K (InjRV x) ∗ is_stack_s l xs.
  Proof.
    iIntros (?) "(#Hinv & Hj & %hd & Hl & %l' & Hhd & Hl')". fold is_list_s. iFrame "Hinv".
    rewrite /is_stack_s /spec_ctx tpool_mapsto_eq /tpool_mapsto_def /=.
    iDestruct "Hinv" as (ρ) "Hinv".
    rewrite heapS_mapsto_eq /heapS_mapsto_def /=.
    iInv specN as (tp σ) ">[Hown %]" "Hclose".
    iDestruct (own_valid_2 with "Hown Hj")
      as %[[?%tpool_singleton_included' _]%prod_included _]%auth_both_valid_discrete.
    iDestruct (own_valid_2 with "Hown Hl")
      as %[[_ Hl%heap_singleton_included]%prod_included _]%auth_both_valid_discrete.
    iDestruct (own_valid_2 with "Hown Hhd")
      as %[[_ Hhd%heap_singleton_included]%prod_included _]%auth_both_valid_discrete.
    iMod (own_update_2 with "Hown Hj") as "[Hown Hj]".
    { by eapply auth_update, prod_local_update_1, singleton_local_update,
        (exclusive_local_update _ (Excl (fill K (InjRV x)))). }
    iMod (own_update_2 with "Hown Hl") as "[Hown Hl]".
    { eapply auth_update, prod_local_update_2.
      apply: singleton_local_update.
      { by rewrite /to_heap lookup_fmap Hl. }
      apply: (exclusive_local_update _
        (1%Qp, to_agree (Some #l' : leibnizO (option val)))).
      apply: pair_exclusive_l. done. }
    iFrame "Hj". iExists l'. iFrame.
    iApply "Hclose". iNext.
    iExists (<[j:=fill K (InjRV x)]> tp), (state_upd_heap <[l:=Some #l']> σ).
    rewrite to_heap_insert to_tpool_insert'; last eauto. iFrame. iPureIntro.
    eapply rtc_r, step_insert_no_fork; eauto. econstructor; eauto.
  Qed.

  Lemma step_rdcss_prim E j K (l_m l_n : loc) (m1 n1 n2 m n : val) b :
    nclose specN ⊆ E →
    vals_compare_safe m m1 → vals_compare_safe n n1 →
    b = bool_decide (m = m1 ∧ n = n1) →
    spec_ctx ∗ j ⤇ fill K (RDCSS_prim #l_m #l_n m1 n1 n2) ∗ l_m ↦ₛ m ∗ l_n ↦ₛ n
    ={E}=∗ spec_ctx ∗ j ⤇ fill K n ∗ l_m ↦ₛ m ∗ l_n ↦ₛ (if b then n2 else n).
  Proof.
    iIntros (? cM cN eqB) "(#Hinv & Hj & Hlm & Hln)". iFrame "Hinv".
    rewrite /spec_ctx tpool_mapsto_eq /tpool_mapsto_def /=.
    iDestruct "Hinv" as (ρ) "Hinv".
    rewrite heapS_mapsto_eq /heapS_mapsto_def /=.
    iInv specN as (tp σ) ">[Hown %]" "Hclose".
    iDestruct (own_valid_2 with "Hown Hj")
      as %[[?%tpool_singleton_included' _]%prod_included _]%auth_both_valid_discrete.
    iDestruct (own_valid_2 with "Hown Hln")
      as %[[_ Hln%heap_singleton_included]%prod_included _]%auth_both_valid_discrete.
    iDestruct (own_valid_2 with "Hown Hlm")
      as %[[_ Hlm%heap_singleton_included]%prod_included _]%auth_both_valid_discrete.
    iMod (own_update_2 with "Hown Hj") as "[Hown Hj]".
    { by eapply auth_update, prod_local_update_1, singleton_local_update,
        (exclusive_local_update _ (Excl (fill K n))). }
    destruct b.
    - iMod (own_update_2 with "Hown Hln") as "[Hown Hln]".
      { eapply auth_update, prod_local_update_2.
        apply: singleton_local_update.
        { by rewrite /to_heap lookup_fmap Hln. }
        apply: (exclusive_local_update _
          (1%Qp, to_agree (Some n2 : leibnizO (option val)))).
        apply: pair_exclusive_l. done. }
      iFrame "Hj". iFrame.
      iApply "Hclose". iNext.
      iExists (<[j:=fill K n]> tp), (state_upd_heap <[l_n:=Some n2]> σ).
      rewrite to_heap_insert to_tpool_insert'; last eauto. iFrame. iPureIntro.
      eapply rtc_r, step_insert_no_fork; eauto. apply (RDCSS_primS _ _ _ _ _ m _ _ true); eauto.
    - iFrame "Hj". iFrame.
      iApply "Hclose". iNext.
      iExists (<[j:=fill K n]> tp), σ.
      rewrite to_tpool_insert'; last eauto. iFrame. iPureIntro.
      eapply rtc_r, step_insert_no_fork; eauto. apply (RDCSS_primS _ _ _ _ _ m _ _ false); eauto.
  Qed.

  Lemma step_resolve_rdcss_prim E j K (l_m l_n : loc) (m1 n1 n2 m n pval : val) b (p : proph_id) :
    nclose specN ⊆ E →
    vals_compare_safe m m1 → vals_compare_safe n n1 →
    b = bool_decide (m = m1 ∧ n = n1) →
    spec_ctx ∗ j ⤇ fill K (Resolve (RDCSS_prim #l_m #l_n m1 n1 n2) #p (of_val pval)) ∗ l_m ↦ₛ m ∗ l_n ↦ₛ n
    ={E}=∗ spec_ctx ∗ j ⤇ fill K n ∗ l_m ↦ₛ m ∗ l_n ↦ₛ (if b then n2 else n).
  Proof.
    iIntros (? cM cN eqB) "(#Hinv & Hj & Hlm & Hln)". iFrame "Hinv".
    rewrite /spec_ctx tpool_mapsto_eq /tpool_mapsto_def /=.
    iDestruct "Hinv" as (ρ) "Hinv".
    rewrite heapS_mapsto_eq /heapS_mapsto_def /=.
    iInv specN as (tp σ) ">[Hown %]" "Hclose".
    iDestruct (own_valid_2 with "Hown Hj")
      as %[[?%tpool_singleton_included' _]%prod_included _]%auth_both_valid_discrete.
    iDestruct (own_valid_2 with "Hown Hln")
      as %[[_ Hln%heap_singleton_included]%prod_included _]%auth_both_valid_discrete.
    iDestruct (own_valid_2 with "Hown Hlm")
      as %[[_ Hlm%heap_singleton_included]%prod_included _]%auth_both_valid_discrete.
    iMod (own_update_2 with "Hown Hj") as "[Hown Hj]".
    { by eapply auth_update, prod_local_update_1, singleton_local_update,
        (exclusive_local_update _ (Excl (fill K n))). }
    destruct b.
    - iMod (own_update_2 with "Hown Hln") as "[Hown Hln]".
      { eapply auth_update, prod_local_update_2.
        apply: singleton_local_update.
        { by rewrite /to_heap lookup_fmap Hln. }
        apply: (exclusive_local_update _
          (1%Qp, to_agree (Some n2 : leibnizO (option val)))).
        apply: pair_exclusive_l. done. }
      iFrame "Hj". iFrame.
      iApply "Hclose". iNext.
      iExists (<[j:=fill K n]> tp), (state_upd_heap <[l_n:=Some n2]> σ).
      rewrite to_heap_insert to_tpool_insert'; last eauto. iFrame. iPureIntro.
      eapply rtc_r, step_insert_no_fork; eauto. econstructor. apply (RDCSS_primS _ _ _ _ _ m _ _ true); eauto.
    - iFrame "Hj". iFrame.
      iApply "Hclose". iNext.
      iExists (<[j:=fill K n]> tp), σ.
      rewrite to_tpool_insert'; last eauto. iFrame. iPureIntro.
      eapply rtc_r, step_insert_no_fork; eauto. econstructor. apply (RDCSS_primS _ _ _ _ _ m _ _ false); eauto.
  Qed.

End rules.
