(* Example assuming a logically atomic implementation of CAS by assuming a refinement with the primitive,
 * which is used to implement a lazy coin, which is shown to refine 
 * a typical eager coin (see the ReLoC / the Future is Ours papers). *)

From reloc.examples.RIL Require Import rdcss_spec adequacy.
From iris.heap_lang Require Import derived_laws.
From reloc Require Import reloc lib.lock.



From reloc.examples Require Import lateearlychoice.

Definition OPTION τ : type := () + τ.

Section coin_example.
  (* Assume a cmpxchg implementation *) 
  Definition cmpxchg_prim : expr := λ: "l" "e1" "e2", CmpXchg "l" "e1" "e2".
  Variable cmpxchg_impl : expr.
  (* Required due to restrictions in ReLoC's context typing *)
  Hypothesis cmpxchg_impl_closed : is_closed_expr ∅ cmpxchg_impl.

  Hypothesis (Hrel : forall `{relocG Σ}, ⊢ inv_heap_inv -∗ REL cmpxchg_impl << cmpxchg_prim : 
    ref (() + lrel_bool) → () + lrel_bool → () + lrel_bool → (() + lrel_bool) * lrel_bool).


  (* coin implementations. *)
  (* Note that these are expressions instead of values. This is to allow binding using ReLoC's contexts *)
  Definition newcoin : expr := λ: <>, ref (rand #()).
  Definition flip : expr := λ: "c", "c" <- rand #().
  Definition read : expr := λ: "c", !"c".

  Definition newcoin_lazy'' : expr := λ: <>, ref NONE.
  Definition read_lazy'' : expr := rec: "read" "c" "p" :=
      match: !"c" with
        InjR "v" => "v"
      | InjL <>  =>
        let: "x" := rand #() in
        if: Snd (Resolve (CmpXchg "c" NONEV (SOME "x")) "p" "x")
        then "x" else "read" "c" "p"
      end.
  Definition flip_lazy'' : expr := λ: "c" "p",
    "c" <- NONE.

  Definition newcoin_lazy' : expr := λ: <>, ref NONE.
  Definition read_lazy' : expr := rec: "read" "c" :=
    match: !"c" with
      SOME "v" => "v"
    | NONE  =>
        let: "x" := rand #() in
        if: Snd (cmpxchg_prim "c" NONEV (SOME "x"))
        then "x"
        else "read" "c"
    end.
  Definition flip_lazy' : expr := λ: "c",
    "c" <- NONE.

  Definition newcoin_lazy : expr := λ: <>, ref NONE.
  Definition read_lazy : expr := rec: "read" "c" :=
    match: !"c" with
      SOME "v" => "v"
    | NONE  =>
        let: "x" := rand #() in
        if: Snd (cmpxchg_impl "c" NONEV (SOME "x"))
        then "x"
        else "read" "c"
    end.
  Definition flip_lazy : expr := λ: "c",
    "c" <- NONE.

  (* eager coin. *)
  Definition coin_eager : expr :=
    let: "c" := newcoin #() in
    ((λ: <>, read "c"), (λ: <>, flip "c")).
  (* intermediate coin 1: annotated with prophecies, using primitive cmpxchg *)
  Definition coin_lazy'' : expr :=
    let: "c" := newcoin_lazy'' #() in
    let: "p" := NewProph in
    ((λ: <>, read_lazy'' "c" "p"), (λ: <>, flip_lazy'' "c" "p")).
  (* intermediate coin 2: no prophecy annotations, still using primitive cmpxchg *)
  Definition coin_lazy' : expr :=
    let: "c" := newcoin_lazy' #() in
    ((λ: <>, read_lazy' "c"), (λ: <>, flip_lazy' "c")).
  (* lazy coin using logically atomic cmpxchg. *)
  Definition coin_lazy : expr :=
    let: "c" := newcoin_lazy #() in
    ((λ: <>, read_lazy "c"), (λ: <>, flip_lazy "c")).


  Fixpoint extract_bool vs :=
    match vs with
    | (PairV _ #(LitBool true), #(LitBool b))::vs => b
    | (PairV _ #(LitBool false), #(LitBool b))::vs => extract_bool vs
    | _ => true
    end.


  Section proofs.
    Context `{!relocG Σ}.

    Definition coinN := nroot.@"coins".

    (* Refinement between intermediate coin 1 and eager coin. Uses prophecies to decide how to step the eager coin. *)
    Lemma coin_lazy_eager :
      ⊢ REL coin_lazy'' << coin_eager : (() → lrel_bool) * (() → ()).
    Proof.
      unfold coin_lazy'', coin_eager, newcoin_lazy'', newcoin.
      rel_pures_l. rel_alloc_l l as "Hl".
      rel_pures_l. rel_newproph_l vs p as "Hp". do 4 rel_pure_l.
      (* use prophecy contents to decide how to step rand *)
      rel_pures_r. rel_apply_r (refines_rand_r (extract_bool vs)).
      rel_alloc_r l' as "Hl'". do 4 rel_pure_r.

      (* allocate invariant to be able to apply refines_arrow_val without loosing assumptions. *)
      iMod (inv_alloc coinN _ (∃ vs, proph p vs ∗ ((l ↦ NONEV ∗ l' ↦ₛ #(extract_bool vs)) ∨ 
          ∃ b : bool, l ↦ SOMEV #b ∗ l' ↦ₛ #b)) with "[-]") as "#Hinv".
      1: eauto with iFrame. 
      iApply refines_pair.
      (* read refinement. *)
      - iApply refines_arrow_val.
        iIntros "!> %v1 %v2 [-> ->]".
        rel_pures_l. unfold read. rel_pures_r. 
        iLöb as "IH".
        unfold read_lazy''. rel_pures_l. fold read_lazy''.
        rel_load_l_atomic.
        iInv coinN as "(%vs' & Hp & [(Hl & Hl')|(%b & Hl & Hl')])" "Hclose".
        + iModIntro. iExists _. iFrame.
          iIntros "!> Hl".
          iMod ("Hclose" with "[-]") as "_"; first eauto with iFrame.
          rel_pures_l.
          rel_apply_l refines_rand_l.
          iIntros "!> %b". rel_pures_l.
          rel_apply_l refines_resolveatomic_l.
          { easy. }
          iInv coinN as "(%vs'' & Hp & [(Hl & Hl')|(%b' & Hl & Hl')])" "Hclose".
          * iModIntro. iExists _. iFrame.
            wp_cmpxchg_suc. iModIntro.
            iIntros (vs''') "-> Hp". simpl.
            rel_pures_l. rel_load_r.
            iMod ("Hclose" with "[-]") as "_"; first eauto 10 with iFrame.
            rel_values.
          * iModIntro. iExists _. iFrame.
            wp_cmpxchg_fail. iModIntro.
            iIntros (vs''') "-> Hp".
            rel_pures_l.
            iMod ("Hclose" with "[-]") as "_"; first eauto 10 with iFrame.
            iApply "IH".
        + iModIntro. iExists _. iFrame.
          iIntros "!> Hl".
          rel_pures_l. rel_load_r.
          iMod ("Hclose" with "[-]") as "_"; first eauto 10 with iFrame.
          rel_values.
      (* flip refinement. *)
      - iApply refines_arrow_val.
        iIntros "!> %v1 %v2 [-> ->]".
        unfold flip_lazy'', flip.
        rel_pures_l. rel_pures_r. 
        rel_store_l_atomic.
        iInv coinN as "(%vs' & Hp & [(Hl & Hl')|(%b & Hl & Hl')])" "Hclose".
        + iModIntro. iExists _. iFrame. 
          iIntros "!> Hl".
          rel_apply_r refines_rand_r.
          rel_store_r.
          iMod ("Hclose" with "[-]") as "_"; first eauto with iFrame.
          rel_values.
        + iModIntro. iExists _. iFrame. 
          iIntros "!> Hl".
          rel_apply_r refines_rand_r.
          rel_store_r.
          iMod ("Hclose" with "[-]") as "_"; first eauto with iFrame.
          rel_values.
    Qed.

    (* refinement between intermediate coin 2 and 1. Removes prophecy annotations. *)
    Lemma coin_lazy'_'' :
      ⊢ REL coin_lazy' << coin_lazy'' : (() → lrel_bool) * (() → ()).
    Proof.
      unfold coin_lazy', coin_lazy'', newcoin_lazy', newcoin_lazy''.
      rel_pures_l. rel_alloc_l l as "Hl". do 4 rel_pure_l.
      rel_pures_r. rel_alloc_r l' as "Hl'". rel_pures_r.
      rel_newproph_r p. do 4 rel_pure_r.
      iMod (inv_alloc coinN _ ((l ↦ NONEV ∗ l' ↦ₛ NONEV) ∨ ∃ b : bool, l ↦ SOMEV #b ∗ l' ↦ₛ SOMEV #b) with "[-]") as "#Hinv".
      1: eauto with iFrame.

      iApply refines_pair.
      - iApply refines_arrow_val.
        iIntros "!> %v1 %v2 [-> ->]".
        rel_pures_l. rel_pures_r. 
        iLöb as "IH".
        unfold read_lazy', read_lazy''.
        rel_pures_l. rel_pures_r. 
        fold read_lazy' read_lazy''.
        rel_load_l_atomic.
        iInv coinN as "[[Hl Hl']|(%b & Hl & Hl')]" "Hclose".
        + iModIntro. iExists _. iFrame.
          iIntros "!> Hl". rel_load_r.
          iMod ("Hclose" with "[-]") as "_"; first eauto with iFrame.
          rel_pures_l. rel_pures_r. 
          rel_apply_l refines_rand_l. iIntros "!> %b".
          rel_apply_r (refines_rand_r b).
          unfold cmpxchg_prim.
          rel_pures_l. rel_pures_r. 
          rel_cmpxchg_l_atomic.
          iInv coinN as "[[Hl Hl']|(%b' & Hl & Hl')]" "Hclose".
          * iModIntro. iExists _. iFrame.
            iSplit; first (iIntros "%H"; done).
            iIntros "_ !> Hl". 
            (* refines_resolve_cmpxchg_*_r were not included with ReLoC. There
             * does not appear to be a general approach to step atomic prophecy 
             * resolutions on the right-hand side of refinements. *)
            rel_apply_r (refines_resolve_cmpxchg_suc_r with "Hl'"); [by econstructor| done|].
            iIntros "Hl'".
            rel_pures_l. rel_pures_r.
            iMod ("Hclose" with "[-]") as "_"; first eauto with iFrame.
            rel_values.
          * iModIntro. iExists _. iFrame.
            iSplit; last (iIntros "%H"; done).
            iIntros "%H !> Hl".
            rel_apply_r (refines_resolve_cmpxchg_fail_r with "Hl'"); [by econstructor| done |].
            iIntros "Hl'".
            rel_pures_l. rel_pures_r.
            iMod ("Hclose" with "[-]") as "_"; first eauto with iFrame.
            iApply "IH".
        + iModIntro. iExists _. iFrame.
          iIntros "!> Hl".
          rel_load_r.
          rel_pures_l. rel_pures_r.
          iMod ("Hclose" with "[-]") as "_"; first eauto with iFrame.
          rel_values.
      - iApply refines_arrow_val.
        iIntros "!> %v1 %v2 [-> ->]".
        unfold flip_lazy', flip_lazy''.
        rel_pures_l. rel_pures_r. 
        rel_store_l_atomic.
        iInv coinN as "[[Hl Hl']|(%b & Hl & Hl')]" "Hclose".
        + iModIntro. iExists _. iFrame. 
          iIntros "!> Hl".
          rel_store_r.
          iMod ("Hclose" with "[-]") as "_"; first eauto with iFrame.
          rel_values.
        + iModIntro. iExists _. iFrame. 
          iIntros "!> Hl".
          rel_store_r.
          iMod ("Hclose" with "[-]") as "_"; first eauto with iFrame.
          rel_values.
    Qed.



    (* Refinement between lazy coin and intermediate coin 2. Replaced the cmpxchg implementation
       by exploiting that refinements are closed under typed contexts. *)
    Lemma coin_lazy_' :
      ⊢ inv_heap_inv -∗ REL coin_lazy << coin_lazy' : (() → lrel_bool) * (() → ()).
    Proof.
      unfold coin_lazy, coin_lazy', read_lazy, read_lazy'. iIntros "#Heap".
      (* We cannot bind things that are underneath a lambda _value_ instead of a lambda expression. *)
      rel_ctx_bind_l cmpxchg_impl.
      rel_ctx_bind_r cmpxchg_prim.
      change (_ * _)%lrel with (interp ((() → TBool) * (() → ()))%ty []).
      iApply refines_related_under_typed_ctx_closed.
      (* This assumption is required to weaken type contexts, cf the report. *)
      - assumption.
      - set_solver.
      - eapply TPCTX_cons.
        { unfold newcoin_lazy'. econstructor. econstructor.
          * econstructor. cbn. apply TAlloc. apply (InjL_typed _ _ TUnit TBool). repeat econstructor.
          * repeat econstructor. }
        eapply TPCTX_cons.
        { econstructor. }
        eapply TPCTX_cons.
        { econstructor. cbn. econstructor. cbn. unfold flip_lazy'.
          econstructor.
          * econstructor. cbn. eapply TStore; first by econstructor.
            repeat econstructor.
          * by econstructor. }
        eapply TPCTX_cons.
        { econstructor. }
        eapply TPCTX_cons.
        { repeat econstructor. }
        eapply TPCTX_cons.
        { econstructor. }
        eapply TPCTX_cons.
        { econstructor; cbn.
          * econstructor. econstructor. done.
          * econstructor. cbn. econstructor. done. }
        eapply TPCTX_cons.
        { econstructor. }
        eapply TPCTX_cons.
        { econstructor. cbn. econstructor.
          * unfold rand. econstructor. econstructor. cbn. econstructor.
            -- econstructor. cbn. econstructor. cbn.
              ++ econstructor. cbn. apply TLoad. econstructor. done.
              ++ apply TFork. eapply TStore.
                ** econstructor. done.
                ** econstructor. econstructor.
            -- repeat econstructor.
          * econstructor. econstructor. }
        eapply TPCTX_cons.
        { econstructor. }
        eapply TPCTX_cons.
        { repeat econstructor. }
        eapply TPCTX_cons.
        { econstructor. }
        eapply TPCTX_cons.
        { econstructor. cbn. apply (InjR_typed _ _ TUnit TBool). by econstructor. }
        eapply TPCTX_cons.
        { cbn. econstructor. cbn. econstructor. eapply (InjL_val_typed _ TUnit TBool). econstructor. }
        eapply TPCTX_cons.
        { repeat econstructor. }
        econstructor.
      - done.
      - cbn. iIntros "!> %Δ". instantiate (1 := OPTION TBool). 
        by iApply Hrel.
    Qed.

  End proofs.


  Lemma ctx_refines_eager_lazy :
    ∅ ⊨ coin_lazy ≤ctx≤ coin_eager : (() → TBool) * (() → ()).
  Proof.
    eapply ctx_refines_transitive.
    { eapply (refines_sound relocΣ _).
      iIntros (? Δ) "#Heap". by iApply coin_lazy_'. }
    eapply ctx_refines_transitive.
    { eapply (refines_sound relocΣ _).
      iIntros (? Δ) "#Heap". by iApply coin_lazy'_''. }
    eapply (refines_sound relocΣ _).
    iIntros (? Δ) "#Heap". by iApply coin_lazy_eager.
  Qed.
End coin_example.

