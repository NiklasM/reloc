(*
  Refinement between the treiber stack from iris/examples and newly added primitives.
  First we show logical refinements for new_stack, push and pop individually
  and then conclude contextual refinements of the corresponding ADTs.

  We both show stack_imp << stack_prim (which is the interesting direction) and stack_prim << stack_imp which is easy and just for fun.
*)

From stdpp Require Import namespaces.
From iris.program_logic Require Export weakestpre atomic.
From iris.heap_lang Require Export lang.
From iris.heap_lang Require Import proofmode notation.
From iris.algebra Require Import frac auth gmap csum.
From iris.prelude Require Import options.


From reloc Require Import reloc.
From reloc.examples.RIL Require Import treiber_spec atomic_utils.

Definition stack_imp : val := Λ: (new_stack, (λ: "s" "x", push "s" "x"), (λ: "s", pop "s")).
Definition stack_prim: val := Λ: (new_stack, (λ: "s" "x", Push_prim "s" "x"), (λ: "s", Pop_prim "s")).

Section semproof.

  Context `{!relocG Σ}.

  (* Require the elements of xs1 and xs2 to be pairwise related having semantic type A *)
  Fixpoint list_sem_type (xs1 xs2 : list val) (A : lrel Σ) :=
      match xs1, xs2 with
      | [], [] => ⌜True⌝%I
      | x1::xs1, x2::xs2 => (A x1 x2 ∗ list_sem_type xs1 xs2 A)%I
      | _,_ => (⌜False⌝)%I
      end.

  Definition stackN := nroot.@"stack".

  Definition stack_inv l1 l2 A : iProp Σ :=
    (∃ xs1 xs2, list_sem_type xs1 xs2 A ∗ is_stack l1 xs1 ∗ is_stack_s l2 xs2)%I.

  Definition stack_type (A : lrel Σ) : lrel Σ := 
    LRel (λ v1 v2, ∃ (l1 l2 : loc), ⌜v1 = #l1⌝ ∗ ⌜v2 = #l2⌝ ∗ inv stackN (stack_inv l1 l2 A))%I.


  Lemma new_stack_refinement A :
    ⊢ REL new_stack << new_stack : () → stack_type A.
  Proof.
    iApply refines_arrow_val. iIntros (s1 s2) "!> (-> & ->)".
    unfold new_stack. rel_pures_l. rel_pures_r.
    rel_alloc_l hd1 as "Hhd1". rel_alloc_l l1 as "Hl1".
    rel_alloc_r hd2 as "Hhd2". rel_alloc_r l2 as "Hl2".
    rel_values. iExists l1,l2. iMod (mapsto_persist with "Hhd1") as "Hhd1".
    iMod (inv_alloc _ _ (stack_inv l1 l2 A) with "[-]"); last by (iModIntro; repeat iSplit).
    iNext. iExists [], []. iSplitR; first done. iSplitL "Hhd1 Hl1"; iExists _; iFrame.
  Qed.

  Lemma push_push_prim_refinement A s1 s2 v1 v2 :
    stack_type A s1 s2 ∗ A v1 v2 ⊢ REL push s1 v1 << Push_prim s2 v2 : ().
  Proof.
    iIntros "((%l1 & %l2 & -> & -> & #Hinv) & #Hval)".

    (* Apply atomic spec *)
    rel_apply_l refines_atomic_l.
    { iApply push_atomic_spec. }

    (* Open invariant *)
    iIntros "!>".
    iInv "Hinv" as (xs1 xs2) "(Hlist & >H1 & >H2)" "Hclose".
    iModIntro. iExists _; iFrame. iSplit.
    - (* Atomic update has been aborted -> close invariant again *)
      iIntros "H1". iApply "Hclose". iNext. iExists _,_. iFrame.
    - (* Atomic update was successful *)
      iIntros "H1".
      (* Perform step on the right side *)
      rel_apply_r (refines_push_prim_r with "H2").
      (* Reestablish invariant *)
      iIntros "H2". iMod ("Hclose" with "[-]") as "_"; first by (iExists _,_; iFrame).
      rel_values.
  Qed.

  (* The other way around - this direction is easy but irrelevant *)
  Lemma push_prim_push_refinement A s1 s2 v1 v2 :
    stack_type A s1 s2 ∗ A v1 v2 ⊢ REL Push_prim s1 v1 << push s2 v2 : ().
  Proof.
    iIntros "((%l1 & %l2 & -> & -> & #Hinv) & #Hval)".

    (* step primitive on the left *)
    rel_apply_l refines_push_prim_l.

    (* open invariant *)
    iInv "Hinv" as (xs1 xs2) "(Hlist & >H1 & >(%hd2' & Hl2 & Hhd2))" "Hclose".
    iExists _. iFrame "H1". iModIntro. iNext. iIntros "H1".

    (* step the implementation on the right *)
    rel_rec_r. rel_pures_r. rel_load_r. rel_pures_r.
    rel_alloc_r l2' as "Hl2'". rel_pures_r. rel_cmpxchg_suc_r. rel_pures_r.
    iAssert (is_stack_s l2 (v2::xs2)) with "[Hhd2 Hl2 Hl2']" as "stack2"; first repeat (iExists _; iFrame).

    (* reestablish invariant *)
    iMod ("Hclose" with "[-]") as "_". { iExists _,_. eauto with iFrame. }
    rel_values.
  Qed.

  Lemma pop_pop_prim_refinement A s1 s2 :
    stack_type A s1 s2 ⊢ REL pop s1 << Pop_prim s2 : () + A.
  Proof.
    iIntros "(%l1 & %l2 & -> & -> & #Hinv)".

    (* Apply the atomic spec *)
    rel_apply_l refines_atomic_l.
    { iApply pop_atomic_spec. }
    
    (* Open invariant *)
    iModIntro.
    iInv "Hinv" as (xs1 xs2) "(Hlist & >H1 & >H2)" "Hclose".
    iModIntro. iExists xs1; iFrame "H1"; simpl.
    iSplit.
    - (* Atomic update has been aborted -> close invariant again *)
      iIntros "H". iMod ("Hclose" with "[-]"); last done.
      iExists _, _. eauto with iFrame.
    - (* Atomic update was successful *)
      (* Case analysis on whether the stack is empty (return None) or not *)
      iIntros "[Hlc H1]". destruct xs1 as [|x1 xs1]; destruct xs2 as [|x2 xs2].
      + (* Is empty -> step on the right *)
        rel_apply_r (refines_pop_prim_empty_r with "H2").
        (* Close invariant *)
        iIntros "H2". iMod ("Hclose" with "[-]") as "_".
        { iExists _, _. eauto with iFrame. }
        rel_values. iExists _, _. auto.
      + by iMod "Hlist". (* Contradiction *)
      + by iMod "Hlist". (* Contradiction *)
      + (* Is not empty -> step on the right *)
        rel_apply_r (refines_pop_prim_succ_r with "H2").
        simpl. iDestruct "Hlist" as "[#Hx Hlist]".
        (* Close invariant *)
        iIntros "H2". iMod ("Hclose" with "[-Hlc]") as "_".
        { iExists _, _. eauto with iFrame. }
        rel_values. 
        iMod (lc_fupd_elim_later with "Hlc Hx") as "#H".
        iModIntro. iExists _, _. eauto.
  Qed.

  (* The other way around - this direction is easy but irrelevant *)
  Lemma pop_prim_pop_refinement A s1 s2 :
    stack_type A s1 s2 ⊢ REL Pop_prim s1 << pop s2 : () + A.
  Proof.
    iIntros "(%l1 & %l2 & -> & -> & #Hinv)".

    (* Step primitive on the left *)
    rel_apply_l refines_pop_prim_l.

    (* Open invariants *)
    iInv "Hinv" as (xs1 xs2) "(Hlist & >H1 & >(%hd2 & Hl2 & Hhd2))" "Hclose".
    iExists _. iFrame "H1". iModIntro. iNext. iIntros "H1".

    (* Step the right and close invariant. Again distinguish whether stack is empty *)
    rel_rec_r. rel_load_r. unfold rec_unfold. rel_pures_r.
    destruct xs1 as [|x1 xs1]; destruct xs2 as [|x2 xs2]; [| done | done |].
    - rel_load_r. rel_pures_r.
      iMod ("Hclose" with "[-]") as "_". { iExists [],[]. iFrame. iNext. iExists _. iFrame. }
      rel_values. iModIntro. rewrite /lrel_car //=. iExists _,_. by iLeft.
    - iDestruct "Hhd2" as (l') "[Hhd2 Hl]". fold is_list_s. rel_load_r. rel_pures_r.
      rel_cmpxchg_suc_r. rel_pures_r. iDestruct "Hlist" as "[Hv Hlist]". fold list_sem_type.
      iMod ("Hclose" with "[Hlist Hl2 Hhd2 Hl H1]") as "_". { iExists xs1,xs2. iFrame. iNext. iExists _. iFrame. }
      rel_values. iModIntro. rewrite /lrel_car //=. iExists _,_. iRight. by iFrame "Hv".
  Qed.

  (* Put logical refinement together *)
  Lemma stack_refinement_imp_prim :
    ⊢ REL stack_imp << stack_prim : ∀ A, ∃ S, (() → S) * (S → A → ()) * (S → () + A).
  Proof.
    unfold stack_imp, stack_prim.
    iApply refines_forall. iIntros (A) "!>".
    iApply (refines_pack (stack_type A)).
    repeat iApply refines_pair.
    - iApply new_stack_refinement.
    - rel_pures_l. rel_pures_r. iApply refines_arrow_val. iIntros (s1 s2) "!> #Hs".
      rel_pures_l. rel_pures_r. iApply refines_arrow_val. iIntros (v1 v2) "!> #Hv".
      rel_pures_l. rel_pures_r. iApply push_push_prim_refinement. iFrame "Hs Hv".
    - rel_pures_l. rel_pures_r. iApply refines_arrow_val. iIntros (s1 s2) "!> #Hs".
      rel_pures_l. rel_pures_r. iApply pop_pop_prim_refinement. iFrame "Hs".
  Qed.

  (* Put logical refinement together *)
  Lemma stack_refinement_prim_imp :
    ⊢ REL stack_prim << stack_imp : ∀ A, ∃ S, (() → S) * (S → A → ()) * (S → () + A).
  Proof.
    unfold stack_imp, stack_prim.
    iApply refines_forall. iIntros (A) "!>".
    iApply (refines_pack (stack_type A)).
    repeat iApply refines_pair.
    - iApply new_stack_refinement.
    - rel_pures_l. rel_pures_r. iApply refines_arrow_val. iIntros (s1 s2) "!> #Hs".
      rel_pures_l. rel_pures_r. iApply refines_arrow_val. iIntros (v1 v2) "!> #Hv".
      rel_pures_l. rel_pures_r. iApply push_prim_push_refinement. iFrame "Hs Hv".
    - rel_pures_l. rel_pures_r. iApply refines_arrow_val. iIntros (s1 s2) "!> #Hs".
      rel_pures_l. rel_pures_r. iApply pop_prim_pop_refinement. iFrame "Hs".
  Qed.

End semproof.

(* Conclude contextual equivalence *)

Definition stack_ADT_type : type := ∀: ∃: (() → #0) * (#0 → #1 → ()) * (#0 → () + #1).
Theorem stack_ctx_refinement_imp_prim :
  ∅ ⊨ stack_imp ≤ctx≤ stack_prim : stack_ADT_type.
Proof.
  eapply (refines_sound relocΣ).
  iIntros (? ?) "_". iApply stack_refinement_imp_prim.
Qed.

Theorem stack_ctx_refinement_prim_imp :
  ∅ ⊨ stack_prim ≤ctx≤ stack_imp : stack_ADT_type.
Proof.
  eapply (refines_sound relocΣ).
  iIntros (? ?) "_". iApply stack_refinement_prim_imp.
Qed.

Theorem stack_ctx_refinement :
  ∅ ⊨ stack_imp =ctx= stack_prim : stack_ADT_type.
Proof.
  split.
  - apply stack_ctx_refinement_imp_prim.
  - apply stack_ctx_refinement_prim_imp.
Qed.
