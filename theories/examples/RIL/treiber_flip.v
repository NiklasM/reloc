(* Example using our refinement between a Treiber stack and equivalent
 * primitives to proof adequacy of a program without opening invariants 
 * around logically atomic operations. *)

From stdpp Require Import namespaces.
From iris.program_logic Require Export weakestpre atomic.
From iris.heap_lang Require Export lang.
From iris.heap_lang Require Import proofmode notation.
From iris.algebra Require Import frac auth gmap csum.
From iris.prelude Require Import options.


From reloc Require Import reloc.
From reloc.examples.RIL Require Import treiber_spec treiber_ADT_refinement atomic_utils adequacy.
From iris.algebra Require Import excl agree csum.



(* Define one shot ghost state *)


Definition one_shotR := csumR (exclR unitO) (agreeR ZO).
Definition Pending : one_shotR := Cinl (Excl ()).
Definition Shot (n : Z) : one_shotR := Cinr (to_agree n).

Class one_shotG Σ := { one_shot_inG : inG Σ one_shotR }.
Local Existing Instance one_shot_inG.

Definition one_shotΣ : gFunctors := #[GFunctor one_shotR].
Global Instance subG_one_shotΣ {Σ} : subG one_shotΣ Σ → one_shotG Σ.
Proof. solve_inG. Qed.


(* Flip example *)
Section example.
  Context `{!heapGS Σ} `{one_shotG Σ}.

  Definition flipN := nroot.@"flip".

  Definition I (γ : gname) (s : loc) : iProp Σ :=
    (∃ vs, is_stack s (#0::vs) ∨ is_stack s (#1::vs) ∨ own γ (Shot 0) ∗ is_stack s vs).

  (* Both programs non-deterministically return 0 or 1 and use a stack as the
  * internal datastructure. The first one used the Treiber stack and the second one uses the primitives. *)
  Definition flip : expr :=
    unpack: "a" := TApp stack_imp in 
      let: "new" := Fst (Fst "a") in 
      let: "push" := Snd (Fst "a") in 
      let: "pop" := Snd "a" in 
        let: "s" := "new" #() in 
        "push" "s" #0 ;; Fork ("push" "s" #1) ;; 
        match: "pop" "s" with
          InjR "r" => "r"
        | InjL <> => #2
        end.
  Definition flip' : expr :=
    unpack: "a" := TApp stack_prim in 
      let: "new" := Fst (Fst "a") in 
      let: "push" := Snd (Fst "a") in 
      let: "pop" := Snd "a" in 
        let: "s" := "new" #() in 
        "push" "s" #0 ;; Fork ("push" "s" #1) ;; 
        match: "pop" "s" with
          InjR "r" => "r"
        | InjL <> => #2
        end.

  (* One property that can be shown of flip'. Note that this proof can also be
  * done for flip directly, but requires opening invariants around logically atomic operations. *)
  Lemma flip'_wp :
    ⊢ WP flip' {{ v, ⌜v = #0 \/ v = #1⌝ }}.
  Proof using All.
    unfold flip', stack_prim, unpack.
    wp_pures. 
    wp_bind (new_stack _). iApply new_stack_spec; first done. iIntros "!> %s Hs". 
    wp_pures.
    wp_bind (Push_prim _ _).
    wp_apply (wp_push_prim with "Hs"). iIntros "Hs".
    wp_pures.

    iMod (own_alloc Pending) as (γ) "Hγ"; first done.
    iMod (inv_alloc flipN _ (I γ s) with "[Hs]") as "#Hinv".
    { iIntros "!>". unfold I. eauto with iFrame. }

    wp_apply wp_fork.
    { wp_pures.
      iInv "Hinv" as (vs) ">[Hs|[Hs|[Hγ Hs]]]".
      all: wp_apply (wp_push_prim with "Hs"); unfold I; eauto 10 with iFrame. }

    wp_pures.
    wp_bind (Pop_prim _). iInv "Hinv" as (vs) ">[Hs|[Hs|[Hγ' Hs]]]".
    - iMod (own_update with "Hγ") as "Hγ".
      { by apply cmra_update_exclusive with (y:=Shot 0). }
      wp_apply (wp_pop_prim with "Hs").
      unfold I. 
      iIntros "Hs !>". 
      iSplitL; first eauto with iFrame.
      wp_pures.  eauto.
    - iMod (own_update with "Hγ") as "Hγ".
      { by apply cmra_update_exclusive with (y:=Shot 0). }
      wp_apply (wp_pop_prim with "Hs").
      unfold I. 
      iIntros "Hs !>". 
      iSplitL; first eauto with iFrame.
      wp_pures.  eauto.
    - by iDestruct (own_valid_2 with "Hγ Hγ'") as %?.
  Qed.
End example.

Definition flipΣ : gFunctors := #[heapΣ; relocΣ; one_shotΣ].

(* Show that flip is correct. *)
Lemma flip_correct σ v t σ' :
  rtc erased_step ([flip], σ) (Val v :: t, σ') -> v = #0 \/ v = #1.
Proof.
  change flip with (fill_ctx [CTX_UnpackL "a" 
           (let: "new" := Fst (Fst "a") in
           let: "push" := Snd (Fst "a") in
           let: "pop" := Snd "a" in
           let: "s" := "new" #() in
           "push" "s" #0;; 
           Fork ("push" "s" #1);; 
           match: "pop" "s" with InjL <> => #2 | InjR "r" => "r" end); CTX_TApp] stack_imp).
  unshelve eapply (wp_ctx_refines_adequacy stack_imp stack_prim stack_ADT_type _ _ TNat) with 
    (φ := fun v => v = #0 \/ v = #1) (Σ := flipΣ).
  - eapply refines_sound' with (Σ := flipΣ); first exact _. iIntros (? ?). iApply stack_refinement_imp_prim.
  - cbn. econstructor.
    2: { repeat econstructor. }
    instantiate (1 := TNat).  
    constructor. asimpl.
    econstructor. 
    2: { eapply Fst_typed, Fst_typed. by constructor. }
    repeat constructor. econstructor. 
    2: { eapply Snd_typed, Fst_typed. by constructor. }
    repeat constructor. econstructor. 
    2: { eapply Snd_typed. by constructor. }
    repeat constructor.
    econstructor. 
    2: { econstructor; last repeat constructor. by constructor. }
    repeat constructor. econstructor.
    2: { repeat econstructor. apply (Nat_val_typed 0). }
    repeat constructor. econstructor.
    2: { apply TFork. repeat econstructor. apply (Nat_val_typed 1). }
    repeat econstructor.
    apply (Nat_val_typed 2).
  - constructor.
  - cbn. intros ?. iApply (wp_stuck_weaken NotStuck).
    unshelve wp_apply flip'_wp.
  - exact _.
Qed.


(* Show that flip is correct and safe. Note that this requires the logical
  * instead of the contextual refinement, because it also includes safety. *)
Lemma flip_adequate σ :
  adequate NotStuck flip σ (fun v _ => v = #0 \/ v = #1).
Proof.
  (* it's probably possible to automate the "bind"(-like) operation here,
  * although this might require changes to reloc. In particular, TApp is just a notation, and not a definition. 
  * Therefore binding cannot decide which context item (an AppL or TApp) to choose.*)
  change flip with (fill_ctx [CTX_UnpackL "a" 
           (let: "new" := Fst (Fst "a") in
           let: "push" := Snd (Fst "a") in
           let: "pop" := Snd "a" in
           let: "s" := "new" #() in
           "push" "s" #0;; 
           Fork ("push" "s" #1);; 
           match: "pop" "s" with InjL <> => #2 | InjR "r" => "r" end); CTX_TApp] stack_imp).
  unshelve eapply (wp_refines_adequacy flipΣ stack_imp stack_prim stack_ADT_type _ _ TNat).
  - iIntros (? ?). iApply stack_refinement_imp_prim.
  - cbn. econstructor.
    2: { repeat econstructor. }
    instantiate (1 := TNat).  
    constructor. asimpl.
    econstructor. 
    2: { eapply Fst_typed, Fst_typed. by constructor. }
    repeat constructor. econstructor. 
    2: { eapply Snd_typed, Fst_typed. by constructor. }
    repeat constructor. econstructor. 
    2: { eapply Snd_typed. by constructor. }
    repeat constructor.
    econstructor. 
    2: { econstructor; last repeat constructor. by constructor. }
    repeat constructor. econstructor.
    2: { repeat econstructor. apply (Nat_val_typed 0). }
    repeat constructor. econstructor.
    2: { apply TFork. repeat econstructor. apply (Nat_val_typed 1). }
    repeat econstructor.
    apply (Nat_val_typed 2).
  - constructor.
  - cbn. intros ?. unshelve wp_apply flip'_wp.
  - exact _.
  - exact _.
Qed.
