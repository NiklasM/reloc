From stdpp Require Import namespaces.
From iris.program_logic Require Export weakestpre atomic.
From iris.heap_lang Require Export lang.
From iris.heap_lang Require Import proofmode notation.
From iris.algebra Require Import frac auth gmap csum.
From iris.prelude Require Import options.


From reloc Require Import reloc.
From iris.algebra Require Import excl agree csum.






(* Contextual refinements transport correctness properties *)
Section wp_refines.
  Variable (e1 e2 : expr) (τ : type).
  Hypothesis (ctx_ref : ctx_refines' ∅ e1 e2 τ).

  Variable (K : ctx) (τ' : type).

  Hypothesis K_typed : typed_ctx K ∅ τ ∅ τ'.
  Hypothesis τ'_eq : EqType τ'.

  Variable φ : val -> Prop.

  Lemma adequacy_ctx_refinement σ :
    adequate MaybeStuck (fill_ctx K e2) σ (fun v _ => φ v) ->
    adequate MaybeStuck (fill_ctx K e1) σ (fun v _ => φ v).
  Proof using All.
    intros [Ha _]. constructor; last easy.
    intros t σ' v.
    intros Hr. 
    eapply ctx_ref in Hr as (t' & σ'' & He2); naive_solver.
  Qed.

  Context `{Σ : gFunctors}.
  Hypothesis fill_wp : forall `{!heapGS Σ}, ⊢ WP fill_ctx K e2 ?{{ v, ⌜φ v⌝ }}.

  Lemma wp_ctx_refines_adequacy `{!heapGpreS Σ} σ  :
    adequate MaybeStuck (fill_ctx K e1) σ (fun v _ => φ v).
  Proof using All.
    eapply adequacy_ctx_refinement, (heap_adequacy Σ).
    iIntros (?) "_". iApply fill_wp.
  Qed.
End wp_refines.




Lemma refinement_bin_log `{relocG Σ} (e e' : expr) Δ (τ : type) :
  ⊢ (REL e << e' : interp τ Δ) ∗-∗ ({⊤;Δ;∅} ⊨ e ≤log≤ e' : τ).
Proof.
  iSplit.
  - iIntros "Hlog" (vs).
    rewrite fmap_empty env_ltyped2_empty_inv.
    iIntros (->).
    rewrite !fmap_empty !subst_map_empty.
    iApply "Hlog".
  - iIntros "Hlog". unfold bin_log_related.
    rewrite !fmap_empty.
    iSpecialize ("Hlog" $! ∅ env_ltyped2_empty).
    by rewrite !fmap_empty !subst_map_empty.
Qed.

Section refines_related_closed.
  Context `{relocG Σ}.

  Variable (e1 e2 : expr).
  Hypothesis (He1 : is_closed_expr ∅ e1) (He2 : is_closed_expr ∅ e2).

  Lemma bin_log_related_weaken_closed Δ Γ τ :
    ({Δ; ∅} ⊨ e1 ≤log≤ e2 : τ) -∗ {Δ; Γ} ⊨ e1 ≤log≤ e2 : τ.
  Proof using All.
    iIntros "H". unfold bin_log_related.
    iIntros (vs) "Hvs".
    rewrite !subst_map_is_closed_empty; try assumption.
    iSpecialize ("H" $! ∅). rewrite fmap_empty.
    iSpecialize ("H" $! env_ltyped2_empty).
    by rewrite !fmap_empty !subst_map_empty.
  Qed.

  Variable (K : ctx) (τ τ' : type) (Γ : stringmap type).
  Hypothesis K_typed : typed_ctx K Γ τ ∅ τ'.

  Lemma refines_related_under_typed_ctx_closed (Δ : list (lrel Σ)) :
    inv_heap_inv -∗
    (□∀ Δ : list (lrel Σ), REL e1 << e2 : interp τ Δ) -∗
    REL fill_ctx K e1 << fill_ctx K e2 : interp τ' Δ.
  Proof using All.
    iIntros "#Hinv #H". iApply refinement_bin_log.
    iPoseProof (bin_log_related_under_typed_ctx _ e1 e2 _ _ _ _ K_typed with "Hinv") as "H'".
    iAssert (□ (∀ Δ0 : list (lrel Σ), {Δ0; Γ} ⊨ e1 ≤log≤ e2 : τ))%I as "H''".
    { iIntros "!> %Δ'". iApply bin_log_related_weaken_closed. iApply refinement_bin_log. iApply "H". }
    iSpecialize ("H'" with "H''").
    iSpecialize ("H'" $! Δ).
    iApply "H'".
  Qed.
End refines_related_closed.
Section refines_related.
  Context `{relocG Σ}.

  Variable (e1 e2 : expr).
  Variable (K : ctx) (τ τ' : type) (Γ : stringmap type).
  Hypothesis K_typed : typed_ctx K ∅ τ ∅ τ'.

  Lemma refines_related_under_typed_ctx (Δ : list (lrel Σ)) :
    inv_heap_inv -∗
    (□∀ Δ : list (lrel Σ), REL e1 << e2 : interp τ Δ) -∗
    REL fill_ctx K e1 << fill_ctx K e2 : interp τ' Δ.
  Proof using All.
    (* TODO this proof didn't come easy, the specializes shoouuld be avoidable *)
    iIntros "#Hinv #H". iApply refinement_bin_log.
    iPoseProof (bin_log_related_under_typed_ctx with "Hinv") as "H'".
    {  eassumption. }
    iAssert (□ (∀ Δ0 : list (lrel Σ), {Δ0; ∅} ⊨ e1 ≤log≤ e2 : τ))%I as "H''".
    { iIntros "!> %Δ'". iApply refinement_bin_log. iApply "H". }
    iSpecialize ("H'" with "H''").
    iSpecialize ("H'" $! Δ).
    iApply "H'".
  Qed.
End refines_related.



(* Logical refinements transport adequacy, i.e. correctness and non-stuckness *)
Section wp_refines.
  Variable (Σ : gFunctors) (e1 e2 : expr)  (τ : type).
  Hypothesis (refinement : forall `{!relocG Σ} Δ, ⊢ REL e1 << e2 : interp τ Δ).

  Variable (K : ctx) (τ' : type).

  Hypothesis K_typed : typed_ctx K ∅ τ ∅ τ'.
  Hypothesis τ'_eq : EqType τ'.


  Variable φ : val -> Prop.

  (* TODO it might be possible to weaken `refinement` to a concrete Delta,
  * although that requires generalising some other lemmas *)
  Lemma adequacy_refinement `{!relocPreG Σ} σ :
    adequate NotStuck (fill_ctx K e2) σ (fun v _ => φ v) ->
    adequate NotStuck (fill_ctx K e1) σ (fun v _ => φ v).
  Proof using All.
    intros [Ha1 Ha2]. constructor.
    - eapply adequacy_ctx_refinement.
      + eapply refines_sound'; eauto.
      + eassumption.
      + eassumption.
      + easy.
    - intros t σ' e' _ Hstep Ht.
      eapply refines_typesafety with (A := fun H => interp τ' []); try eassumption.
      iIntros (?) "#Heap".
      iApply refinement_bin_log.
      iAssert (□ (∀ Δ : list (lrel Σ), {Δ; ∅} ⊨ e1 ≤log≤ e2 : τ))%I as "H".
      { iIntros "!>" (Δ). iApply refinement_bin_log. iApply refinement. }
      iApply (bin_log_related_under_typed_ctx with "Heap H").
      assumption.
  Qed.

  Hypothesis fill_wp : forall `{!heapGS Σ}, ⊢ WP fill_ctx K e2 {{ v, ⌜φ v⌝ }}.

  Lemma wp_refines_adequacy `{!heapGpreS Σ} `{!relocPreG Σ} σ  :
    adequate NotStuck (fill_ctx K e1) σ (fun v _ => φ v).
  Proof using All.
    eapply adequacy_refinement, (heap_adequacy Σ).
    iIntros (?) "_". iApply fill_wp.
  Qed.
End wp_refines.
