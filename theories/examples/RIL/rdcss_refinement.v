(* 
  Refinement between the RDCSS operations and newly added primitives. We first
   show refinements for each of the operations, and then combine them to obtain
   a full refinement.

  Note that we do not show the "boring" refinement here. We expect this to be
   easy.
*)

From reloc Require Import logic.proofmode.spec_tactics.
From reloc.logic Require Import model spec_rules.


From iris.proofmode Require Import
     coq_tactics ltac_tactics
     sel_patterns environments
     reduction.


From stdpp Require Import namespaces.
From iris.program_logic Require Export weakestpre atomic.
From iris.heap_lang Require Export lang.
From iris.heap_lang Require Import proofmode notation.
From iris.algebra Require Import excl auth agree frac list cmra csum gmap.
From iris.prelude Require Import options.


From reloc Require Import reloc.
From reloc.examples.RIL Require Import rdcss_spec atomic_utils.
From reloc.prelude Require Import properness asubst.


Section semproof.

  Context {Σ} `{!rdcssG Σ} `{!relocG Σ}.

  Definition N := nroot .@ "rdcss_semproof".
  Definition Ntype := nroot .@ "sem_type".

  (* require v1 be of the rdcss datastructure, and v2 to be a pointer to a related value. *)
  Definition rdcss_type (A : lrel Σ) : lrel Σ := 
    LRel (λ v1 v2, ∃ (l1 l2 : loc), ⌜v1 = #l1⌝ ∗ ⌜v2 = #l2⌝ ∗ is_rdcss N l1
            ∗ inv Ntype (∃ n1 n2, A n1 n2 ∗ rdcss_state N l1 n1 ∗ l2 ↦ₛ n2))%I.

  (* We show this refinement directly since there is no interesting atomic spec for allocating an RDCSS *)
  Lemma new_rdcss_refinement (A : lrel Σ) :
    inv_heap_inv ⊢ REL new_rdcss << new_RDCSS_p : A → rdcss_type A.
  Proof.
    iIntros "#invHeap".
    iApply refines_arrow_val.
    iIntros (n1 n2) "!> An".
    rel_apply_l refines_wp_l.
    wp_apply (new_rdcss_spec N with "invHeap").
    - solve_ndisj.
    - done.
    - iIntros (l1) "[is Hl1]".
      unfold new_RDCSS_p. rel_alloc_r l2 as "Hl2".
      rel_values. iMod (inv_alloc _ _ (∃ n1 n2, A n1 n2 ∗ rdcss_state N l1 n1 ∗ l2 ↦ₛ n2) with "[An Hl1 Hl2]"); first eauto with iFrame. iModIntro.
      iExists l1, l2. by repeat (iSplit; first done).
  Qed.


  Lemma get_rdcss_refinement (A : lrel Σ) :
    ⊢ REL rdcss_spec.get << get_RDCSS_p : rdcss_type A → A.
  Proof.
    iApply refines_arrow_val.
    iIntros (v1 v2) "!> (%l1 & %l2 & -> & -> & His & #Hinv)".

    (* Apply atomic spec on the left *)
    iPoseProof (get_spec' _ A with "His") as "H".
    rel_apply_l (refines_atomic_l' with "H").

    (* open invariants *)
    iIntros "!>". iInv Ntype as "(%n1 & %n2 & #An & >H1 & >H2)" "Hclose".
    iModIntro.
    iExists n1, n2. iFrame "An H1". iSplit.
    - (* AU abort -> restore invariant with old resources *)
      iIntros "H1". by iMod ("Hclose" with "[H1 H2 An]"); first eauto with iFrame.
    - (* resources have been updated *)
      iIntros "[H1 H3]".
      (* step on the right and update the right resources *)
      iIntros (j) "Hj". tp_rec j. tp_load j.
      (* close invariant again *)
      iMod ("Hclose" with "[H3 H2 An]") as "_"; first eauto with iFrame.
      iModIntro. iApply wp_value. iExists _. iFrame.
  Qed.

  Definition eq_lrel (A : lrel Σ) := ∀ E v1 v2 w1 w2, ↑relocN.@"ref" ⊆ E -> A v1 v2 -∗ A w1 w2 -∗ |={E}=> ⌜v1 = w1 <-> v2 = w2⌝.
  Definition unboxed_lrel (A : lrel Σ) := ∀ v1 v2, A v1 v2 -∗ ⌜val_is_unboxed v1 ∧ val_is_unboxed v2⌝.
  Definition unboxed_lrel_InjL1 (A : lrel Σ) := ∀ v1 v2, A v1 v2 -∗ ⌜val_is_unboxed (InjLV v1)⌝.

  Lemma rdcss_refinement (A B : lrel Σ) (l_m l_m' l_n l_n' : loc) (m1 m1' n1 n1' n2 n2' : val) :
    eq_lrel A -> eq_lrel B ->
    unboxed_lrel A -> unboxed_lrel B -> 
    unboxed_lrel_InjL1 A →
    B m1 m1' -∗ A n1 n1' -∗ A n2 n2' -∗
    inv (relocN.@"ref".@(l_m, l_m')) (∃ v1 v2 : val, l_m ↦_(λ _ : val, True) v1 ∗ l_m' ↦ₛ v2 ∗ B v1 v2) -∗
    rdcss_type A #l_n #l_n' -∗
    REL rdcss      #l_m  #l_n  m1 n1 n2 <<
        RDCSS_prim #l_m' #l_n' m1' n1' n2' : A.
  Proof.
    iIntros (Aeq Beq Aun Bun AunInj) "#Hm1 #Hn1 #Hn2 #Hl_m (%ln & %ln' & -> & -> & #Hln1 & #Hln2)".
    iPoseProof (Bun with "Hm1") as "[%Hm1 %Hm1']".
    iPoseProof (Aun with "Hn1") as "[_ %Hn1']".
    iPoseProof (AunInj with "Hn1") as "%Hn1''".

    (* Apply atomic spec on the left *)
    rel_apply_l refines_atomic_l'; first by iApply rdcss_spec.
    iModIntro.

    (* Open invariants *)
    iInv Ntype as "(%l_nv & %l_nv' & #Hl_nA & >Hl_nr & >Hl_n'p)" "HcloseNtype". 
    { unfold Ntype, N, inv_heapN. rewrite <-difference_difference. solve_ndisj. }
    iInv "Hl_m" as (l_mv l_m'v) "(>Hl_mp & >Hl_m'p & #Hl_mB)" "HcloseN".
    { rewrite <-difference_difference. solve_ndisj. }
    iModIntro. iExists _, _. iSplitL "Hl_mp Hl_nr".
    { iFrame. }
    iSplit.
    - (* AU abort -> restore invariant with old resources *)
      iIntros "(Hl_mp & Hl_nr)".
      iMod ("HcloseN" with "[Hl_mp Hl_m'p]"); first eauto with iFrame.
      iMod ("HcloseNtype" with "[Hl_nr Hl_n'p]"); first eauto with iFrame.
      done.
    - (* resources have been updated *)
      iIntros "(Hlc & Hl_mp & Hl_nr)".
      (* use later credit *)
      iAssert (▷ (A l_nv l_nv' ∗ B l_mv l_m'v))%I as "Hlater"; first eauto.
      iMod (lc_fupd_elim_later with "Hlc Hlater") as "[#Hl_nA' #Hl_mB']".
      (* step on the right and update the right resources *)
      iCombine "Hl_m'p Hl_n'p" as "Hl_mn'p".
      iIntros (j) "Hj". tp_rdcss_prim j.
      iDestruct "Hl_mn'p" as "[Hl_m'p Hl_n'p]".
      (* close invariants with updated resources *)
      iMod ("HcloseN" with "[Hl_mp Hl_m'p]") as "_"; first eauto with iFrame.
      iMod (Aeq with "Hn1 Hl_nA'") as "%HA".
      { rewrite <-difference_difference. solve_ndisj. }
      iMod (Beq with "Hm1 Hl_mB'") as "%HB".
      { rewrite <-difference_difference. solve_ndisj. }
      iMod ("HcloseNtype" with "[Hl_nr Hl_n'p]") as "_".
      { iNext. iExists _, _. iFrame. rewrite bool_decide_decide.
        destruct decide as [[-> ->]|H1]; destruct decide as [[-> ->]|H2]; naive_solver. }
      iModIntro. wp_pures. eauto with iFrame.
  Qed.


  (* Note that this reifnement is at type refA instead of ref. This is due to
  * the invariant points-to in the rdcss spec, which required us to add a new type to ReLoC. *)
  Lemma rdcss_refinement_imp_prim (A B : lrel Σ) :
    eq_lrel A -> eq_lrel B ->
    unboxed_lrel A -> unboxed_lrel B ->
    unboxed_lrel_InjL1 A ->
    inv_heap_inv -∗ 
    REL rdcss_imp << rdcss_prim : ∃ refA, (A → refA) * (refA → A) * (refTrue B → refA → B → A → A → A).
  Proof using rdcssG0 relocG0 Σ.
    iIntros (Aeq Beq Aun Bun AunInj) "#invHeap".
    unfold rdcss_prim, rdcss_imp.
    iApply (refines_pack (rdcss_type A)).
    repeat iApply refines_pair.
    - iApply new_rdcss_refinement. eauto with iFrame.
    - iApply get_rdcss_refinement.
    - iApply refines_arrow_val. 
      iIntros (s1 s2) "!> (%l_m & %l_m' & -> & -> & #Heap & #Hl_m)".
      rel_pures_l. rel_pures_r. iApply refines_arrow_val.
      iIntros (s1 s2) "!> (%l_n & %l_n' & -> & -> & #Hl_nr & #Hl_ni)".
      rel_pures_l. rel_pures_r. iApply refines_arrow_val.
      iIntros (m1 m1') "!> #Hm1".
      rel_pures_l. rel_pures_r. iApply refines_arrow_val.
      iIntros (m2 m2') "!> #Hm2".
      rel_pures_l. rel_pures_r. iApply refines_arrow_val.
      iIntros (n2 n2') "!> #Hn2".
      rel_pures_l. rel_pures_r.

      iApply rdcss_refinement; try done.
      iExists _, _. eauto with iFrame.
  Qed.

End semproof.

Definition rdcss_ADT_type α β : type := ∃: (α → #0) * (#0 → α) * (TRefTrue β → #0 → β → α → α → α).

Import uPred.

Lemma rdcss_ADT_type_subst (H0 : relocG #[ relocΣ; rdcssΣ]) α β Δ :
  @interp (gFunctors.app relocΣ (gFunctors.app rdcssΣ gFunctors.nil)) H0 (rdcss_ADT_type α.[ren(+1)] β.[ren(+1)]) Δ
≡ (∃ τ : lrel #[ relocΣ; rdcssΣ], (interp α Δ → τ) * (τ → interp α Δ) *
  (refTrue (interp β Δ) → τ → interp β Δ → interp α Δ → interp α Δ → interp α Δ))%lrel.
Proof.
  cbn. intros v1 v2. unfold lrel_car. simpl.
  apply equiv_dist=> n.
  apply exist_ne=> τ. apply equiv_dist. f_equiv.
  by setoid_rewrite interp_1.
Qed.


Theorem rdcss_ctx_refinement_imp_prim α β :
  UnboxedType α → UnboxedType β →
  ∅ ⊨ rdcss_imp ≤ctx≤ rdcss_prim : rdcss_ADT_type α.[ren(+1)] β.[ren(+1)].
Proof.
  intros Uα Uβ.
  eapply (refines_sound #[relocΣ; rdcssΣ]).
  iIntros (? ?) "Heap". setoid_rewrite rdcss_ADT_type_subst.
  iApply (rdcss_refinement_imp_prim with "Heap").
  - intros ?????. by apply unboxed_type_eq.
  - intros ?????. by apply unboxed_type_eq.
  - intros ??. by apply unboxed_type_sound.
  - intros ??. by apply unboxed_type_sound.
  - intros ? ?.
    induction Uα; simpl;
    first [iDestruct 1 as (? ?) "[% [% ?]]"
          |iDestruct 1 as (?) "[% %]"
          |iIntros "[% %]"];
    simplify_eq/=; eauto with iFrame.
Qed.
