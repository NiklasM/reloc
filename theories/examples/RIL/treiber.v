(*
  This is old code! Please see treiber_ADT_refinement.v

  (Problem was that pop is not logical atomic in general but requires the stack to be read only.
   We solve this in treiber_ADT_refinement.v by hiding the it in a ADT.)
*)

From stdpp Require Import namespaces.
From iris.program_logic Require Export weakestpre atomic.
From iris.heap_lang Require Export lang.
From iris.heap_lang Require Import proofmode notation.
From iris.algebra Require Import frac auth gmap csum.
From iris.prelude Require Import options.


From reloc Require Import reloc.
From reloc.examples.RIL Require Import treiber_spec atomic_utils.

Definition OPTION τ : type := () + τ.
Definition stack_type_inner τ : type := (OPTION (τ.[ren (+1)] * ref #0)).
Definition stack_type_syn τ : type :=  ref ref (μ: stack_type_inner τ).


Lemma new_stack_type τ :
  ∅ ⊢ₜ new_stack : (() → stack_type_syn τ).
Proof. repeat constructor. Qed.

Lemma push_type τ :
  ∅ ⊢ₜ push : (stack_type_syn τ → τ → ()).
Proof.
  repeat constructor.
  econstructor.
  2: { apply TLoad. constructor. set_solver. }
  constructor. econstructor.
  2: { apply TAlloc. apply TFold with (τ := stack_type_inner τ). 
    repeat constructor. asimpl. constructor. }
  constructor. constructor.
  - econstructor. constructor.
    2-4: constructor; set_solver.
    constructor.
  - repeat constructor.
  - repeat (econstructor + set_solver).
Qed.
Lemma pop_type τ :
  ∅ ⊢ₜ pop : (stack_type_syn τ → OPTION τ).
Proof. 
  repeat constructor.
  econstructor.
  2: { eapply TLoad. constructor. set_solver. }
  constructor. econstructor.
  - apply TUnfold with (τ := stack_type_inner τ). apply TLoad. constructor. set_solver.
  - repeat constructor.
  - repeat constructor.
    + econstructor. constructor.
      2-4: (repeat econstructor); set_solver.
      constructor.
    + econstructor. constructor. asimpl. set_solver.
    + repeat econstructor.
Qed.

Section semproof.

  Context `{!relocG Σ}.

  Fixpoint list_sem_type (xs1 xs2 : list val) (A : lrel Σ) :=
      match xs1, xs2 with
      | [], [] => ⌜True⌝%I
      | x1::xs1, x2::xs2 => (A x1 x2 ∗ list_sem_type xs1 xs2 A)%I
      | _,_ => (⌜False⌝)%I
      end.

  Lemma sem_type_cons E (hd1 hd2 l1' l2' : loc) v1 v2 τ Δ: 
    ▷interp τ Δ v1 v2 ∗ ▷l1' ↦ InjRV(v1, #hd1) ∗ ▷l2' ↦ₛ InjRV(v2, #hd2)
    ∗ ▷inv (relocN.@"ref".@(hd1, hd2)) (∃ v1 v2, hd1 ↦ v1 ∗ hd2 ↦ₛ v2 ∗ interp (μ: stack_type_inner τ) Δ v1 v2)%I
    ⊢ |={E}=> inv (relocN.@"ref".@(l1', l2')) (∃ v1 v2, l1' ↦ v1 ∗ l2' ↦ₛ v2 ∗ interp (μ: stack_type_inner τ) Δ v1 v2)%I.
  Proof.
    iIntros "(Hv & Hl1' & Hl2' & Hinv)".
    iApply inv_alloc. iNext.
    iExists _,_. iFrame. simpl.
    erewrite lrel_rec_unfold.
    simpl. rewrite {1}/lrel_rec1 {4}/lrel_car. simpl. iNext.
    iExists (v1, #hd1)%V,(v2, #hd2)%V. iRight. iSplit; first done. iSplit; first done.
    iExists v1, v2, #hd1, #hd2. iSplit; first done. iSplit; first done.
    iSplit; first last.
    - iExists hd1, hd2. iSplit; first done. iSplit; first done. iFrame.
    - specialize (interp_ren_up [] Δ τ) as eq. erewrite eq. iFrame "Hv".
  Qed.

  Lemma sem_stack_match Δ τ v1 v2 :
    interp (μ: stack_type_inner τ) Δ v1 v2 
  ⊢ ▷(⌜v1 = InjLV #()⌝ ∗ ⌜v2 = InjLV #()⌝
    ∨ ∃ (v v' l1 l2 : val), ⌜v1 = InjRV (v, l1)%V⌝ ∗ ⌜v2 = InjRV (v', l2)%V⌝ ∗ interp (ref μ: stack_type_inner τ) Δ l1 l2).
  Proof.
    rewrite //= {1}lrel_rec_unfold /lrel_rec1 /lrel_rec1 {1}/lrel_car //=.
    iIntros "(%v & %v' & [(>-> & >-> & [>-> >->]) | (>-> & >-> & H)])"; iNext; first by iLeft. 
    iRight.
    iDestruct "H" as (v1 v2 v1' v2') "(-> & -> & [H H'])".
    iExists _,_,_,_. do 2 (iSplit; first done). iFrame.
  Qed.

  Lemma refines_push_l K E l (v:val) (t : expr) A :
    □ (|={⊤,E}=> ∃ (hd: loc), l ↦ #hd ∗
       (l ↦ #hd ={E,⊤}=∗ True) ∧
        ((∃ (l': loc), l ↦ #l' ∗ l' ↦ InjRV(v, #hd)%V) -∗
        REL fill K (of_val #()) << t @ E : A))
    -∗ REL fill K (push #l v) << t : A.
  Proof.
    iApply (refines_atomic_l $! (push_atomic_spec' _ _)).
  Qed.

  Lemma push_semtype τ Δ :
    ⊢ REL (λ: "s" "x", push "s" "x")%V << (λ: "s" "x", push "s" "x")%V : (interp (stack_type_syn τ) Δ) → (interp τ Δ) → ().
  Proof.
    iApply refines_arrow_val. cbn.
    iIntros (s1 s2) "!> (%l1 & %l2 & -> & -> & #Inv)".
    rel_pures_l. rel_pures_r.
    iApply refines_arrow_val.
    iIntros (v1 v2) "!> #Tv".
    rel_pures_l. rel_pures_r.
    unfold push. rel_pures_l. do 2 rel_pure_r. fold push.
    iLöb as "IH".

    rel_load_l_atomic. iInv "Inv" as (? ?) "(Hl1 & Hl2 & %hd1 & %hd2 & >-> & >-> & Hrec)" "Hclose".
    iExists #hd1. iFrame.
    iModIntro. iNext. iIntros "Hl1".
    (*rel_load_r.*)
    iMod ("Hclose" with "[-]") as "_"; first by repeat (iExists _,_; iFrame).
    clear hd2.

    rel_pures_l. (*rel_pures_r.*)
    rel_alloc_l l1' as "Hl1'".
    (*rel_alloc_r l2' as "Hl2'".*)
    rel_pures_l. (*rel_pures_r.*)

    rel_cmpxchg_l_atomic.
    iInv "Inv" as (? ?) "(Hl1 & Hl2 & %hd1' & %hd2 & >-> & >-> & Hrec)" "Hclose".
    iExists _. iFrame "Hl1".
    iModIntro. iSplit.
    - (* CmpXchg fails *)
      iIntros (neq); iNext; iIntros "Hl1".
      iMod ("Hclose" with "[-]") as "_"; first by repeat (iExists _,_; iFrame).
      rel_pures_l. rel_rec_l. rel_pures_l. iApply "IH".
    - (* CmpXchg succeeds *)
      iIntros (?). simplify_eq/=. iNext. iIntros "Hl1". rel_pures_l.

      rel_load_r. rel_pures_r. rel_alloc_r l2' as "Hl2'". rel_pures_r.
      rel_cmpxchg_suc_r. rel_pures_r.

      iMod (sem_type_cons with "[Tv Hl1' Hl2' Hrec]") as "Hrec"; first by eauto with iFrame.
      iMod ("Hclose" with "[-]") as "_"; first by repeat (iExists _,_; iFrame).
      rel_values.

    Restart.

    iApply refines_arrow_val. cbn.
    iIntros (s1 s2) "!> (%l1 & %l2 & -> & -> & #Inv)".
    rel_pures_l. rel_pures_r.
    iApply refines_arrow_val.
    iIntros (v1 v2) "!> #Tv".
    rel_pures_l. rel_pures_r.

    assert (push #l1 v1 = fill [] (push #l1 v1)) as -> by reflexivity.
    iApply (refines_push_l).
    iIntros "!>".
    iInv "Inv" as (vl1 vl2) "(>Hl1 & >Hl2 & (%hd1 & %hd2 & >-> & >-> & Hrec))" "Hclose".
    iModIntro. iExists hd1. iFrame. iSplit.
    - iIntros. iApply "Hclose". iNext. iExists _,_. iFrame.
      rewrite /lrel_car. iExists hd1, hd2. by iFrame "Hrec".
    - iIntros "(%l1' & Hl1 & Hl1')".

      unfold push.
      rel_load_r. rel_pures_r. rel_alloc_r l2' as "Hl2'". rel_pures_r.
      rel_cmpxchg_suc_r. rel_pures_r.

      iMod (sem_type_cons with "[Tv Hl1' Hl2' Hrec]") as "Hrec"; first by eauto with iFrame.
      iMod ("Hclose" with "[-]") as "_"; first by repeat (iExists _,_; iFrame).
      rel_values.
  Qed.

  Lemma push_prim_semtype τ Δ :
    ⊢ REL (λ: "s" "x", Push_prim "s" "x")%V << (λ: "s" "x", Push_prim "s" "x")%V : (interp (stack_type_syn τ) Δ) → (interp τ Δ) → ().
  Proof.
    iApply refines_arrow_val. unfold lrel_car. unfold stack_type_syn.
    Opaque stack_type_inner. cbn.
    iIntros (s1 s2) "!> (%l1 & %l2 & -> & -> & #Inv)".
    unfold lrel_car. cbn.
    rel_pures_l. rel_pures_r.
    iApply refines_arrow_val.
    iIntros (v1 v2) "!> #Tv".
    rel_pures_l. rel_pures_r.

    assert (Push_prim #l1 v1 = fill [] (Push_prim #l1 v1)) as -> by reflexivity.
    iApply refines_push_prim_l'.
    iInv "Inv" as (? ?) "(>Hl1 & Hl2 & %hd1' & %hd2 & >-> & >-> & Hrec)" "Hclose".
    iExists _. iFrame "Hl1". iModIntro. iNext. iIntros (l1') "Hl1 Hl1'".

    assert (Push_prim #l2 v2 = fill [] (Push_prim #l2 v2)) as -> by reflexivity.
    iApply (refines_push_prim_r' with "Hl2"); first solve_ndisj.
    iIntros "(%l2' & Hl2 & Hl2')".

    iMod (sem_type_cons with "[Tv Hl1' Hl2' Hrec]") as "Hrec"; first by eauto with iFrame.
    iMod ("Hclose" with "[-]") as "_"; first by repeat (iExists _,_; iFrame).
    rel_values.
  Qed.

  Lemma push_prim_push_refinement τ Δ :
    ⊢ REL (λ: "s" "x", Push_prim "s" "x")%V << push : (interp (stack_type_syn τ) Δ) → (interp τ Δ) → ().
  Proof.
    iApply refines_arrow_val. rewrite /lrel_car /stack_type_syn //=.
    iIntros (s1 s2) "!> (%l1 & %l2 & -> & -> & #Inv)".
    unfold lrel_car, push. cbn.
    rel_pures_l. rel_pures_r. fold push.
    iApply refines_arrow_val.
    iIntros (v1 v2) "!> #Tv".
    rel_pures_l. rel_pures_r.
    
    assert (Push_prim #l1 v1 = fill [] (Push_prim #l1 v1)) as -> by reflexivity.
    iApply refines_push_prim_l'.
    iInv "Inv" as (? ?) "(>Hl1 & Hl2 & %hd1' & %hd2 & >-> & >-> & Hrec)" "Hclose".
    iExists _. iFrame "Hl1". iModIntro. iNext. iIntros (l1') "Hl1 Hl1'".

    rel_load_r. rel_pures_r. rel_alloc_r l2' as "Hl2'". rel_pures_r.
    rel_cmpxchg_suc_r. rel_pures_r.

    iMod (sem_type_cons with "[Tv Hl1' Hl2' Hrec]") as "Hrec"; first by eauto with iFrame.
    iMod ("Hclose" with "[-]") as "_"; first by repeat (iExists _,_; iFrame).
    rel_values.
  Qed.

  Lemma push_push_prim_refinement τ Δ :
    ⊢ REL push << (λ: "s" "x", Push_prim "s" "x")%V : (interp (stack_type_syn τ) Δ) → (interp τ Δ) → ().
  Proof.
    iApply refines_arrow_val. unfold lrel_car. unfold stack_type_syn.
    remember (TRec (stack_type_inner τ)) as τrec. cbn.
    iIntros (s1 s2) "!> (%l1 & %l2 & -> & -> & #Inv)".
    unfold lrel_car. cbn.
    unfold push.
    rel_pures_l. rel_pures_r. fold push.
    iApply refines_arrow_val.
    iIntros (v1 v2) "!> #Tv".
    rel_pures_l. rel_pures_r. iLöb as "IH".

    rel_load_l_atomic. iInv "Inv" as (? ?) "(>Hl1 & Hl2 & %hd1 & %hd2 & >-> & >-> & Hrec)" "Hclose".
    iExists _. iFrame.
    iModIntro. iNext. iIntros "Hl1".
    iMod ("Hclose" with "[-]") as "_"; first by repeat (iExists _,_; iFrame).
    clear hd2.

    rel_pures_l.
    rel_alloc_l l1' as "Hl1'".
    rel_pures_l.

    rel_cmpxchg_l_atomic.
    iInv "Inv" as (? ?) "(>Hl1 & Hl2 & %hd1' & %hd2 & >-> & >-> & Hrec)" "Hclose".
    iExists _. iFrame "Hl1".
    iModIntro. iSplit.
    - (* CmpXchg fails *)
      iIntros (neq); iNext; iIntros "Hl1".
      iMod ("Hclose" with "[-]") as "_"; first by repeat (iExists _,_; iFrame).
      rel_pures_l. rel_rec_l. rel_pures_l. iApply "IH".
    - (* CmpXchg succeeds *)
      iIntros (?). simplify_eq/=. iNext. iIntros "Hl1". rel_pures_l.

      assert (Push_prim #l2 v2 = fill [] (Push_prim #l2 v2)) as -> by reflexivity.
      iApply (refines_push_prim_r' with "Hl2"); first solve_ndisj.
      iIntros "(%l2' & Hl2 & Hl2')".
  
      iMod (sem_type_cons with "[Tv Hl1' Hl2' Hrec]") as "Hrec"; first by eauto with iFrame.
      iMod ("Hclose" with "[-]") as "_"; first by repeat (iExists _,_; iFrame).
      rel_values.
  Qed.

  Lemma sem_stack_match' Δ τ v1 v2 :
    interp (μ: stack_type_inner τ) Δ v1 v2 
  ⊢ ▷(⌜v1 = InjLV #()⌝ ∗ ⌜v2 = InjLV #()⌝
    ∨ ∃ (v v' l1 l2 : val), ⌜v1 = InjRV (v, l1)%V⌝ ∗ ⌜v2 = InjRV (v', l2)%V⌝ ∗ interp (ref μ: stack_type_inner τ) Δ l1 l2).
  Proof.
    rewrite //= {1}lrel_rec_unfold /lrel_rec1 /lrel_rec1 {1}/lrel_car //=.
    iIntros "(%v & %v' & [(>-> & >-> & [>-> >->]) | (>-> & >-> & H)])"; iNext; first by iLeft. 
    iRight.
    iDestruct "H" as (v1 v2 v1' v2') "(-> & -> & [H H'])".
    iExists _,_,_,_. do 2 (iSplit; first done). iFrame.
  Qed.

  Lemma refines_pop_l P K E l (v1 v2 : val) t A τ Δ :
    P -∗
    □ (|={⊤,E}=> ∃ (hd: loc), l ↦ #hd ∗ hd ↦v1 ∗ interp (μ: stack_type_inner τ) Δ v1 v2 ∗
       (l ↦ #hd -∗ hd ↦v1 -∗ interp (μ: stack_type_inner τ) Δ v1 v2 ={E,⊤}=∗ True)
      ∧ (⌜v1 = InjLV #()⌝ -∗ ⌜v2 = InjLV #()⌝ -∗ l ↦ #hd -∗ hd ↦v1 -∗ interp (μ: stack_type_inner τ) Δ v1 v2
        -∗ REL fill K (of_val #()) << t @ E : A)
      ∧ (∀ (v v' l1 l2 : val), ⌜v1 = InjRV (v, l1)%V⌝ -∗ ⌜v2 = InjRV (v', l2)%V⌝ -∗ interp (ref μ: stack_type_inner τ) Δ l1 l2
         -∗ l ↦ l1 -∗ REL fill K (of_val #()) << t @ E : A))
    -∗ REL fill K (pop #l) << t : A.
  Proof.
    iIntros "P #H".
    iLöb as "IH".
    unfold pop, rec_unfold.
    rel_pures_l. fold pop.
    iPoseProof "H" as "H'". (* lolwhat *)
    rel_load_l_atomic. iMod "H" as "(%hd & Hl & Hhd & Hv & [close _])".
    iModIntro. iExists _. iFrame. iIntros "!> Hl".
    iMod ("close" with "Hl Hhd Hv") as "_".
    rel_pures_l.
    iPoseProof "H'" as "H". (* lolwhat *)
    rel_load_l_atomic. iMod "H" as "(%hd' & Hl & Hhd & Hv & [close _])".
    iModIntro. iExists v1.
  Abort.

  Lemma pop_semtype τ Δ :
    ⊢ REL pop << pop : (interp (stack_type_syn τ) Δ) → (interp (OPTION τ) Δ).
  Proof.
    iApply refines_arrow_val. unfold lrel_car. unfold stack_type_syn.
    remember (TRec (stack_type_inner τ)) as τrec. cbn.
    iIntros (s1 s2) "!> (%l1 & %l2 & -> & -> & #Inv)".
    unfold pop.
    rel_pures_l. rel_pures_r. fold pop.
    iLöb as "IH".

    rel_pures_l. rel_pures_r.
    rel_load_l_atomic. iInv "Inv" as (? ?) "(Hl1 & >Hl2 & %hd1 & %hd2 & >-> & >-> & #Hrec)" "Hclose".
    iExists #hd1. iFrame.
    iModIntro. iNext. iIntros "Hl1".
    rel_load_r.
    iMod ("Hclose" with "[Hl1 Hl2]") as "_"; first by repeat (iExists _,_; iFrame; eauto).

    rel_pures_l. rel_load_l_atomic.
    rewrite {2}Heqτrec.
    iInv "Hrec" as (v1 v2) "(Hl1 & Hl2 & #Hv)" "Hclose".
    iExists v1. iFrame.
    iModIntro. iNext. iIntros "Hl1".
    unfold rec_unfold.
    rel_pures_r. rel_load_r.
    iMod ("Hclose" with "[Hl1 Hl2]") as "_"; first repeat (iExists _,_; iFrame; eauto).

    iDestruct (sem_stack_match with "Hv") as "[H | H]"; unfold rec_unfold; rel_pure_l; rel_pure_r.
    - iDestruct "H" as "(-> & ->)".
      rel_pures_l. rel_pures_r. rel_values.
      iModIntro. rewrite /lrel_car //=. iExists _,_. by iLeft.
    - iDestruct "H" as (v v' l1' l2') "(-> & -> & H)".
      rel_pures_l. rel_pures_r.

      rel_cmpxchg_l_atomic.
      iInv "Inv" as (? ?) "(Hl1 & Hl2 & %hd1' & %hd2' & >-> & >-> & Hrec')" "Hclose".
      iExists _. iFrame "Hl1".
      iModIntro. iSplit.
      + (* CmpXchg fails *)
        iIntros (neq); iNext; iIntros "Hl1".
        rel_cmpxchg_fail_r.
  (* TODO *)
  Abort.

  Theorem pop_prim_ctx_euqiv N τ l :
    inv N (∃ xs, is_stack l xs) ⊢ ⌜∅ ⊨ Pop_prim #l =ctx= pop #l : OPTION τ⌝.
  Proof.
    iIntros "Hinv".
    iSplit.
    - iPureIntro. eapply (refines_sound relocΣ).
      iIntros (? ?).
  Abort.
End semproof.

Theorem push_push_prim_ctx_refinement τ :
  ∅ ⊨ push ≤ctx≤ (λ: "s" "x", Push_prim "s" "x")%V : stack_type_syn τ → τ → ().
Proof.
  eapply (refines_sound relocΣ).
  iIntros (? ?) "_". iApply push_push_prim_refinement.
Qed.

Theorem push_prim_push_ctx_refinement τ :
  ∅ ⊨ (λ: "s" "x", Push_prim "s" "x")%V ≤ctx≤ push : stack_type_syn τ → τ → ().
Proof.
  eapply (refines_sound relocΣ).
  iIntros (? ?) "_". iApply push_prim_push_refinement.
Qed.

Theorem push_prim_ctx_equiv τ :
  ∅ ⊨ (λ: "s" "x", Push_prim "s" "x")%V =ctx= push : stack_type_syn τ → τ → ().
Proof. split; first apply push_prim_push_ctx_refinement; apply push_push_prim_ctx_refinement. Qed.
