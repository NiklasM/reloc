(*
  Copied from https://gitlab.mpi-sws.org/iris/examples/-/blob/b93ed6be72319079eb9f4f94372d3d7e4cb4229c/theories/logatom/treiber.v
   and modified slightly.
*)

From stdpp Require Import namespaces.
From iris.program_logic Require Export weakestpre atomic.
From iris.heap_lang Require Export lang.
From iris.heap_lang Require Import proofmode notation.
From iris.algebra Require Import frac auth gmap csum.
From iris.prelude Require Import options.
From iris.base_logic.lib Require Import invariants.
From iris.bi.lib Require Import atomic.

Definition rec_unfold : val := λ: "x", "x".

Definition new_stack: val := λ: <>, ref (ref NONE).

Definition push: val :=
  rec: "push" "s" "x" :=
      let: "hd" := !"s" in
      let: "s'" := ref (SOME ("x", "hd")) in
      if: CAS "s" "hd" "s'"
        then #()
        else "push" "s" "x".
Definition pop: val :=
  rec: "pop" "s" :=
      let: "hd" := !"s" in
      match: rec_unfold !"hd" with
        SOME "cell" =>
        if: CAS "s" "hd" (Snd "cell")
          then SOME (Fst "cell")
          else "pop" "s"
      | NONE => NONE
      end.

Definition iter: val :=
  rec: "iter" "hd" "f" :=
      match: !"hd" with
        NONE => #()
      | SOME "cell" => "f" (Fst "cell") ;; "iter" (Snd "cell") "f"
      end.

Notation is_list := is_list_prim.
Notation is_stack := is_stack_prim.

Section proof.
  Context `{!heapGS Σ} (N: namespace).

  Global Instance is_list_persistent hd xs : Persistent (is_list hd xs).
  Proof. revert hd. induction xs; simpl; apply _. Qed.

  Lemma uniq_is_list xs ys hd : is_list hd xs ∗ is_list hd ys ⊢ ⌜xs = ys⌝.
  Proof.
    revert ys hd. induction xs as [|x xs' IHxs']; intros ys hd.
    - induction ys as [|y ys' IHys'].
      + auto.
      + iIntros "[Hxs Hys] /=".
        iDestruct "Hys" as (hd') "[Hhd Hys']".
        by iDestruct (mapsto_agree with "Hxs Hhd") as %?.
    - induction ys as [|y ys' IHys'].
      + iIntros "[Hxs Hys] /=".
        iDestruct "Hxs" as (?) "[Hhd _]".
        by iDestruct (mapsto_agree with "Hhd Hys") as %?.
      + iIntros "[Hxs Hys] /=".
        iDestruct "Hxs" as (?) "[Hhd Hxs']".
        iDestruct "Hys" as (?) "[Hhd' Hys']".
        iDestruct (mapsto_agree with "Hhd Hhd'") as %[= -> ->].
        by iDestruct (IHxs' with "[$Hxs' $Hys']") as %->.
  Qed.

  Lemma new_stack_spec:
    {{{ True }}} new_stack #() {{{ s, RET #s; is_stack s [] }}}.
  Proof.
    iIntros (Φ) "_ HΦ". wp_lam.
    wp_bind (ref NONE)%E. wp_alloc l as "Hl".
    iMod (mapsto_persist with "Hl") as "Hl".
    wp_alloc l' as "Hl'".
    iApply "HΦ". rewrite /is_stack. auto.
  Qed.

  Lemma push_atomic_spec (s: loc) (x: val) :
    ⊢ <<< ∀∀ (xs : list val), is_stack s xs >>>
        push #s x @ ∅
      <<< is_stack s (x::xs), RET #() >>>.
  Proof.
    unfold is_stack.
    iIntros (Φ) "HP". iLöb as "IH". wp_rec.
    wp_let. wp_bind (! _)%E.
    iMod "HP" as (xs) "[Hxs [Hvs' _]]".
    iDestruct "Hxs" as (hd) "[Hs Hhd]".
    wp_load. iMod ("Hvs'" with "[Hs Hhd]") as "HP"; first by eauto with iFrame.
    iModIntro. wp_let. wp_alloc l as "Hl". wp_let.
    iMod (mapsto_persist with "Hl") as "Hl".
    wp_bind (CmpXchg _ _ _)%E.
    iMod "HP" as (xs') "[Hxs' Hvs']".
    iDestruct "Hxs'" as (hd') "[Hs' Hhd']".
    destruct (decide (hd = hd')) as [->|Hneq].
    * wp_cmpxchg_suc. iDestruct "Hvs'" as "[_ Hvs']".
      iMod ("Hvs'" with "[-]") as "HQ".
      { simpl. by eauto 10 with iFrame. }
      iModIntro. wp_pures. eauto.
    * wp_cmpxchg_fail.
      iDestruct "Hvs'" as "[Hvs' _]".
      iMod ("Hvs'" with "[-]") as "HP"; first by eauto with iFrame.
      iModIntro. wp_pures. by iApply "IH".
  Qed.

  Lemma push_atomic_spec' (l: loc) (v: val) :
    ⊢ <<< ∀∀ (hd : loc), l ↦ #hd >>>
        push #l v @ ∅
      <<< ∃ (l': loc), l ↦ #l' ∗ l' ↦ InjRV(v, #hd)%V, RET #()>>>.
  Proof.
    iIntros (Φ) "AU". iLöb as "IH". wp_rec.
    wp_let. wp_bind (! _)%E.
    iMod "AU" as (hd) "[Hl [Hvs' _]]".
    wp_load. iMod ("Hvs'" with "Hl") as "AU".
    iModIntro. wp_let.
    
    wp_alloc l' as "Hl'". wp_let.
    wp_bind (CmpXchg _ _ _)%E.
    iMod "AU" as (hd') "[Hl AU]".
    destruct (decide (hd = hd')) as [->|Hneq].
    * wp_cmpxchg_suc. iDestruct "AU" as "[_ AU]".
      iMod ("AU" with "[-]") as "AU".
      { simpl. by eauto 10 with iFrame. }
      iModIntro. wp_pures. eauto.
    * wp_cmpxchg_fail.
      iDestruct "AU" as "[AU _]".
      iMod ("AU" with "[-]") as "HP"; first by eauto with iFrame.
      iModIntro. wp_pures. by iApply "IH".
  Qed.

  Lemma push_nonatomic_spec (s: loc) (x: val) :
    ⊢ {{{ inv N (∃ xs, is_stack s xs )%I }}} push #s x {{{ RET  #(); True }}}.
  Proof.
    iIntros (Φ) "!> #Hinv HΦ".
    awp_apply push_atomic_spec.
    iDestruct (inv_acc with "Hinv") as "H"; first done.
    iInv N as (xs) ">Hs".
    iAaccIntro with "Hs".
    - iIntros "Hs !>". iFrame. by iExists _.
    - iIntros "Hs !>". iSplitL "Hs"; first by iExists _.
      iAssert (▷⌜True⌝)%I as "true"; first done.
      iDestruct ("HΦ" with "true") as "HΦ".
      iClear "Hinv H true".
      (* TODO *)
  Abort.

  (* Showing the pop refinement requires the specification to return a later
   * credit. The modification required was easy. *)
  Lemma pop_atomic_spec (s: loc) :
    ⊢ <<< ∀∀ (xs : list val), is_stack s xs >>>
        pop #s @ ∅
      <<< £ 1 ∗ match xs with [] => is_stack s []
                  | x::xs' => is_stack s xs' end,
      RET match xs with [] => NONEV | x :: _ => SOMEV x end >>>.
  Proof.
    unfold is_stack.
    iIntros (Φ) "HP". iLöb as "IH". 
    unfold pop. wp_pure credit:"Hlc". fold pop.
    wp_bind (! _)%E.
    iMod "HP" as (xs) "[Hxs Hvs']". simpl.
    iDestruct "Hxs" as (hd) "[Hs #Hhd]".
    destruct xs as [|y' xs'].
    - simpl. wp_load. iDestruct "Hvs'" as "[_ Hvs']".
      iMod ("Hvs'" with "[-]") as "HQ".
      { eauto with iFrame. }
      iModIntro. wp_let. wp_load. wp_lam. wp_pures.
      eauto.
    - simpl. iDestruct "Hhd" as (hd') "(Hhd & Hxs')".
      wp_load. iDestruct "Hvs'" as "[Hvs' _]".
      iMod ("Hvs'" with "[-Hlc]") as "HP".
      { iFrame. eauto with iFrame. }
      iModIntro. wp_let. wp_load. wp_rec. wp_match. wp_proj.
      wp_bind (CmpXchg _ _ _).
      iMod "HP" as (xs'') "[Hxs'' Hvs']".
      iDestruct "Hxs''" as (hd'') "[Hs'' Hhd'']".
      destruct (decide (hd = hd'')) as [->|Hneq].
      + wp_cmpxchg_suc. iDestruct "Hvs'" as "[_ Hvs']".
        destruct xs'' as [|x'' xs'']; simpl.
        { by iDestruct (mapsto_agree with "Hhd Hhd''") as %?. }
        iDestruct "Hhd''" as (hd''') "[Hhd'' Hxs'']".
        iDestruct (@mapsto_agree with "Hhd Hhd''") as %[= -> ->].
        iMod ("Hvs'" with "[-]") as "HQ".
        { eauto with iFrame. }
        iModIntro. wp_pures. eauto.
      + wp_cmpxchg_fail. iDestruct "Hvs'" as "[Hvs' _]".
        iMod ("Hvs'" with "[-]") as "HP"; first by eauto with iFrame.
        iModIntro. wp_pures. by iApply "IH".
  Qed.
End proof.
