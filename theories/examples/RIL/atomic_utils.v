From stdpp Require Import namespaces.
From iris.program_logic Require Export weakestpre atomic.
From iris.heap_lang Require Export lang.
From iris.heap_lang Require Import proofmode notation.
From iris.algebra Require Import frac auth gmap csum.
From iris.prelude Require Import options.
From iris.bi.lib Require Import atomic.

From reloc Require Import reloc.

(*
  We need our own notation for atomic triples as the default notation conflicts with autosubst
  see https://gitlab.mpi-sws.org/iris/iris/-/issues/473
*)

Notation "'<<<<' ∀∀ x1 .. xn , α '>>>>' e @ E '<<<<' β , 'RET' v '>>>>'" :=
  (atomic_wp (TA:=TeleS (λ x1, .. (TeleS (λ xn, TeleO)) .. ))
             (TB:=TeleO)
             e%E
             E
             (tele_app $ λ x1, .. (λ xn, α%I) ..)
             (tele_app $ λ x1, .. (λ xn, tele_app β%I) .. )
             (tele_app $ λ x1, .. (λ xn, tele_app v%V) .. )
  )
  (at level 20, E, α, β, v at level 200, x1 binder, xn binder,
   format "'[hv' '<<<<'  '[' ∀∀  x1  ..  xn ,  '/' α  ']' '>>>>'  '/  ' e  @  E  '/' '<<<<'  '[' β ,  '/' 'RET'  v  ']' '>>>>' ']'")
  : bi_scope.

Section utils.
  Context `{!relocG Σ} {TA TB : tele}.
  Notation iProp := (iProp Σ).
  Implicit Types (α : TA → iProp) (β : TA → TB → iProp) (f : TA → TB → val).

  (*
    Lemma for applying atomic Hoare triples on the left side of a logical refinement.
    Basically, this Lemma states that Iris's notion of logical atomicity entails the TaDA-style notion of relational atomicity from the ReLoC paper
    https://iris-project.org/pdfs/2021-lmcs-reloc-reloaded-final.pdf#subsubsection.5.3.3

    Because we allow a general mask E' here instead of ∅, the definition of the logical refinement needs to be unfolded here allowing the ={E, ⊤ ∖ E'}=∗ instead of ={E, ⊤}=∗.
    See: Definiton of `refines_def` in theories/logic/model.v
  *)

  Lemma refines_atomic_l' K E E' e1 (t : expr) (A : lrel Σ) α β f:
    atomic_wp e1 E' α β f
    ⊢
    □ (|={⊤ ∖ E',E}=> ∃.. x, α x ∗
      (α x ={E,⊤ ∖ E'}=∗ True) ∧
      (∀.. y, β x y -∗ ∀ j, refines_right j t ={E, ⊤ ∖ E'}=∗ WP fill K (of_val $ f x y) {{ v, ∃ v' : val, refines_right j v' ∗ A v v' }}))
    -∗ REL fill K e1 << t : A.
  Proof.
    iIntros "spec #H". iApply refines_split. iIntros (k) "Hk".
    rel_apply_l refines_wp_l.
    awp_apply "spec".
    unfold atomic_acc.
    iApply fupd_trans.
    iMod "H" as "(%x & Px & H)".
    iModIntro.
    iExists x. iFrame "Px".
    iApply fupd_mask_intro; first set_solver.
    iIntros "em". iSplit.
    - iIntros "Px". iDestruct "H" as "[H _]".
      iSpecialize ("H" with "Px").
      iApply fupd_trans.
      iMod "em". iMod "H".
      done.
    - iIntros (y) "Qxv".
      iDestruct "H" as "[_ H]".
      iDestruct ("H" with "Qxv") as "H".
      iMod "em".
      rewrite refines_eq /refines_def. simpl.
      iMod ("H" $! k with "Hk") as "H".
      iApply fupd_mask_intro; first set_solver.
      rewrite ->!tele_app_bind.
      iIntros "em'" (j <-).
      by iModIntro.
  Qed.

  (*
    In the special case of E' = ∅, logical atomicity entails the exact TaDA-style notion of relational atomicity found in the ReLoC paper.
  *)

  Lemma refines_atomic_l K E e1 (t : expr) (A : lrel Σ) α β f:
    atomic_wp e1 ∅ α β f
    ⊢
    □ (|={⊤,E}=> ∃.. x, α x ∗
      (α x ={E,⊤}=∗ True) ∧
        (∀.. y, β x y -∗
        REL fill K (of_val $ f x y) << t @ E : A))
    -∗ REL fill K e1 << t : A.
  Proof.
    iIntros "A #H".
    iPoseProof (refines_atomic_l' with "A") as "A".
    iApply "A". iIntros "!>". iMod (fupd_mask_subseteq ⊤) as "Hemp"; first set_solver.
    iMod "H" as "(%x & Hx & H)".
    iModIntro. iExists x. iFrame. iSplit.
    - iIntros "Px". iApply fupd_trans. iMod ("H" with "Px"). by iModIntro.
    - iDestruct "H" as "[_ H]". 
      rewrite refines_eq /refines_def. 
      iIntros (y) "Q %j Hr". iSpecialize ("H" with "Q Hr").
      iMod "H". iMod "Hemp". iModIntro. iFrame.
  Qed.

End utils.
